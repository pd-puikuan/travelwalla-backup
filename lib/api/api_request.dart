import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../utils/constants.dart';

class ApiRequest {
  static post({port, uri, postBody, successAction}) async {
    print('Request: ${Constants.apiBaseUrl}$uri');
    var headers = {'Content-Type': 'application/json'};
    var request = http.Request(
        'POST',
        Uri.parse(
            '${Constants.apiBaseUrl}$uri'));
    request.body =
        json.encode(postBody);
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();
    if (response.statusCode == 200 || response.statusCode == 201) {
      print('Request Success');
      var data = await response.stream.bytesToString();
      successAction(data);
    } else {
      var data = await response.stream.bytesToString();
      var info = json.decode(data) as Map<String, dynamic>;
      var errorList = info["error"]["details"]["errors"];
      var errorHeader = info["error"]["message"];
      var errorMessages = [];
      for (var i = 0; i < errorList.length; i++) {
        errorMessages.add(errorList[i]['message']);
      }
      throw ErrorHint(errorMessages.join(", "));
    }
  }

   static get({port, uri, params, successAction}) async {
    print('Request: ${Constants.apiBaseUrl}$uri$params');
    var headers = {'Content-Type': 'application/json'};
    var request = http.Request(
        'GET',
        Uri.parse(
            '${Constants.apiBaseUrl}$uri$params'));
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();
    if (response.statusCode == 200 || response.statusCode == 201) {
      
      var data = await response.stream.bytesToString();
      print('Request Success $data');
      successAction(data);
    } else {
      print('${response.reasonPhrase}');
    }
  }
}
