import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:firebase_core/firebase_core.dart';
import 'firebase_options.dart';

import 'models/search.dart';
import 'models/email.dart';
import 'models/phone.dart';
import 'models/password.dart';
import 'models/otp.dart';
import 'models/social.dart';
import 'models/hotel.dart';
import 'screens/home/home.dart';

import 'screens/login/login_signin.dart';
import 'screens/login/login_signup.dart';
import 'screens/login/login_signup_success.dart';
import 'screens/login/login_otp.dart';
import 'screens/login/login_reset.dart';
import 'screens/login/login_reset_confirmation.dart';

import 'screens/hotel/hotel_listing.dart';
import 'screens/hotel/hotel_details.dart';
import 'screens/hotel/hotel_search.dart';
import 'screens/hotel/hotel_booking.dart';
import 'screens/hotel/hotel_confirmation.dart';
import 'screens/hotel/hotel_payment.dart';

import 'screens/hotel/search/search_rooms.dart';
import 'screens/hotel/search/search_dates.dart';
import 'screens/hotel/search/search_location.dart';

import 'screens/hotel/details/details_room.dart';
import 'screens/hotel/details/details_gallery.dart';
import 'screens/hotel/details/details_facilities.dart';
import 'screens/hotel/details/details_reviews.dart';
import 'screens/hotel/details/details_location.dart';

import 'screens/flight/flight_search.dart';

import 'screens/search/sort.dart';
import 'screens/search/filter.dart';
import 'screens/others/downtime.dart';

import 'utils/strings.dart';
import 'utils/routes.dart';
import 'utils/theme.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(MultiProvider(providers: [
    ChangeNotifierProvider(create: (context) => SearchModel()),
    ChangeNotifierProvider(create: (context) => SocialModel()),
    ChangeNotifierProvider(create: (context) => EmailModel()),
    ChangeNotifierProvider(create: (context) => PhoneModel()),
    ChangeNotifierProvider(create: (context) => OtpModel()),
    ChangeNotifierProvider(create: (context) => PasswordModel()),
    ChangeNotifierProvider(create: (context) => HotelModel()),
  ], child: const MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: Strings.title,
        theme: AppTheme().themedata,
        initialRoute: Routes.home,
        routes: {
          Routes.home: (context) => const HomeScreen(),
          Routes.loginSignin: (context) => const SigninScreen(),
          Routes.loginSignup: (context) => const SignupScreen(),
          Routes.loginSignupSuccess: (context) => const SignupSuccessScreen(),
          Routes.loginReset: (context) => const LoginResetScreen(),
          Routes.loginResetConfirmation: (context) =>
              const LoginResetConfirmationScreen(),
          Routes.loginOtp: (context) => const LoginOtpScreen(),
          Routes.hotelListing: (context) => const HotelListingScreen(),
          Routes.hotelSearch: (context) => const HotelSearchScreen(),
          Routes.hotelDetails: (context) => HotelDetailsScreen(),
          Routes.hotelBooking: (context) => const HotelBookingScreen(),
          Routes.hotelConfirmation: (context) =>
              const HotelConfirmationScreen(),
          Routes.hotelPayment: (context) => const HotelPaymentScreen(),
          Routes.detailsRoom: (context) => DetailsRoomScreen(),
          Routes.detailsGallery: (context) => DetailsGalleryScreen(),
          Routes.detailsFacilities: (context) => DetailsFacilitiesScreen(),
          Routes.detailsReviews: (context) => const DetailsReviewsScreen(),
          Routes.detailsLocation: (context) => const DetailsLocationScreen(),
          Routes.searchRooms: (context) => const SearchRoomsScreen(),
          Routes.searchDates: (context) => const SearchDatesScreen(),
          Routes.searchLocation: (context) => const SearchLocationScreen(),
          Routes.flightSearch: (context) => const FlightSearchScreen(),
          Routes.filter: (context) => const FilterScreen(),
          Routes.sort: (context) => const SortScreen(),
          Routes.downtime: (context) => const DowntimeScreen(),
        });
  }
}
