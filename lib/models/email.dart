import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../api/api_request.dart';
import '../../utils/api.dart';
import '../../utils/routes.dart';

import 'alert.dart';
import 'password.dart';

class EmailModel extends ChangeNotifier {
  late TextEditingController _emailController;

  bool _emailLogin = true;

  TextEditingController get emailController => _emailController;
  bool get emailLogin => _emailLogin;

  void initEmailLoginController() {
    _emailController = TextEditingController(text: '');
  }

  void disposeEmailLoginController() {
    _emailController.dispose();
  }

  void updatePlatform() {
    _emailLogin = !emailLogin;
    notifyListeners();
  }

  void updateFormValue(controller, newValue) {
    _emailController = TextEditingController(text: newValue);
  }

  directToOtp(context) {
    var passwordController =
        Provider.of<PasswordModel>(context, listen: false).passwordController;
    Navigator.pushNamed(context, Routes.loginOtp, arguments: {
      'email': emailController.text,
      'password': passwordController.text,
      'medium': 'email',
    });
  }

  loginAccount(context) async {
    var passwordController =
        Provider.of<PasswordModel>(context, listen: false).passwordController;
    try {
      await ApiRequest.post(
          // port: '3004',
          uri: API.login,
          postBody: {
            "identifier": emailController.text,
            'password': passwordController.text,
          },
          successAction: (_) => Navigator.pushNamed(context, Routes.home));
    } catch (e) {
      Alert().showMyDialog(context, e);
    }
  }

  checkAccount(context) async {
    try {
      await ApiRequest.post(
          // port: '3004',
          uri: API.checkExistingAccount,
          postBody: {
            "email": emailController.text,
          },
          successAction: (_) => registerAccount(context));
    } catch (e) {
      Alert().showMyDialog(context, e);
    }
  }

  registerAccount(context) async {
    var passwordController =
        Provider.of<PasswordModel>(context, listen: false).passwordController;
    try {
      await ApiRequest.post(
          uri: API.registerUser,
          postBody: {
            "email": emailController.text,
            'password': passwordController.text,
          },
          successAction: (_) =>
              Navigator.pushNamed(context, Routes.loginSignupSuccess));
    } catch (e) {
      Alert().showMyDialog(context, e);
    }
  }

  forgotPassword(context) async {
    try {
      await ApiRequest.post(
          uri: API.forgotPassword,
          postBody: {
            "channel": "email",
            "email": emailController.text,
          },
          successAction: (_) =>
              Navigator.pushNamed(context, Routes.loginOtp, arguments: {
                'email': emailController.text,
                'medium': 'email',
              }));
    } catch (e) {
      Alert().showMyDialog(context, e);
    }
  }
}
