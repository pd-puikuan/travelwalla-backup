import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../api/api_request.dart';
import '../../utils/api.dart';
import '../../utils/routes.dart';

import 'alert.dart';
import 'search.dart';

class HotelModel extends ChangeNotifier {
  getHotelList(context) async {
    final searchModel = Provider.of<SearchModel>(context, listen: false);
    var checkInDate = searchModel.hotelDateStart;
    var checkOutDate = searchModel.hotelDateEnd;
    var adults = searchModel.adultCount;
    var children = searchModel.childrenCount;
    var infants = searchModel.infantCount;
    var rooms = searchModel.roomCount;
    var location = Uri.encodeQueryComponent(searchModel.goingToController.text);

    var params =
        'checkInDate=$checkInDate&checkOutDate=$checkOutDate&adults=$adults&childs=$children&infants=$infants&room=$rooms&start=0&limit=20&location=$location';
    
    try {
      await ApiRequest.get(
          uri: API.hotelList,
          params: params,
          successAction: (response) =>
              Navigator.pushNamed(context, Routes.hotelListing, arguments: {
                'data': response,
              }));
    } catch (e) {
      Alert().showMyDialog(context, e);
    }
  }
}
