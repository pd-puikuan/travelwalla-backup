import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'search.dart';
import 'place_service.dart';
import '../../utils/constants.dart';

class LocationSearch extends SearchDelegate<Suggestion> {
  LocationSearch(this.sessionToken) {
    apiClient = PlaceApiProvider(sessionToken);
  }

  final sessionToken;
  PlaceApiProvider apiClient = PlaceApiProvider('');

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        tooltip: 'Clear',
        icon: const Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      tooltip: 'Back',
      icon: const Icon(Icons.close),
      onPressed: () {
        Navigator.of(context)
          ..pop()
          ..pop();
        // close(context, Suggestion('', ''));
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return const SizedBox();
  }

  _generateLocationText(context, data, index) {
    return InkWell(
      onTap: () {
        Provider.of<SearchModel>(context,listen: false).updateHistory(data[index], context);
      },
      child: Expanded(
        child: Column(
          children: [
            SizedBox(
              height: 50,
              child: Row(
                  children: [
                    const Icon(Icons.undo),
                    const SizedBox(width: 10),
                    Expanded(
                      child: Text(
                        data[index],
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                      ),
                    )
                  ],
                ),
              ),
            const Divider(),
          ],
        ),
      ),
    );
  }

  _checkDataExists(snapshot) {
    switch (snapshot.connectionState) {
      case ConnectionState.none:
        return const Text('Begin search...');
      case ConnectionState.active:
      case ConnectionState.waiting:
        return const Text('Loading...');
      case ConnectionState.done:
        // print('Done ${snapshot.data}');
        var data = [];
        for (var i = 0; i < snapshot.data.length; i++) {
          data.add((snapshot.data[i] as Suggestion).description);
        }
        return ListView.builder(
          itemBuilder: (context, index) => _generateLocationText(context, data, index),
          itemCount: snapshot.data.length,
        );
    }
  }

  _fetchSuggestions(context) {
    if (query.length < Constants.minQueryLength) {
      return null;
    } else {
      apiClient = PlaceApiProvider(sessionToken);
      return apiClient
          .fetchSuggestions(query, Localizations.localeOf(context).languageCode)
          .then((onValue) {
        // print('On Value: $onValue');
        return onValue;
      });
    }
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return FutureBuilder(
      future: _fetchSuggestions(context),
      builder: (context, snapshot) => query.length < Constants.minQueryLength
          ? Container(
              child: Provider.of<SearchModel>(context,listen: false).generateInitialView(context),
            )
          : Container(
              child: _checkDataExists(snapshot)),
    );
  }
}
