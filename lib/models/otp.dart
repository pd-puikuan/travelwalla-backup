import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../api/api_request.dart';
import '../../utils/api.dart';
import '../../utils/routes.dart';

import 'phone.dart';
import 'email.dart';

class OtpModel extends ChangeNotifier {
  late TextEditingController _otp1Controller;
  late TextEditingController _otp2Controller;
  late TextEditingController _otp3Controller;
  late TextEditingController _otp4Controller;
  late TextEditingController _otp5Controller;
  late TextEditingController _otp6Controller;

  String _otpNumber = '000000';
  TextEditingController get otp1Controller => _otp1Controller;
  TextEditingController get otp2Controller => _otp2Controller;
  TextEditingController get otp3Controller => _otp3Controller;
  TextEditingController get otp4Controller => _otp4Controller;
  TextEditingController get otp5Controller => _otp5Controller;
  TextEditingController get otp6Controller => _otp6Controller;

  String get otpNumber => _otpNumber;

  void initOtpController() {
    _otp1Controller = TextEditingController(text: '');
    _otp2Controller = TextEditingController(text: '');
    _otp3Controller = TextEditingController(text: '');
    _otp4Controller = TextEditingController(text: '');
    _otp5Controller = TextEditingController(text: '');
    _otp6Controller = TextEditingController(text: '');
  }

  void disposeOtpController() {
    _otp1Controller.dispose();
    _otp2Controller.dispose();
    _otp3Controller.dispose();
    _otp4Controller.dispose();
    _otp5Controller.dispose();
    _otp6Controller.dispose();
  }

  combineOtpNumbers() {
    List<String> otpSet = [
      _otp1Controller.text,
      _otp2Controller.text,
      _otp3Controller.text,
      _otp4Controller.text,
      _otp5Controller.text,
      _otp6Controller.text
    ];
    _otpNumber = otpSet.join();
  }

  verifyOtp(arguments, context) {
    combineOtpNumbers();
    var phoneBody = {
      "phoneNumber": arguments['phoneNumber'],
      "phoneCode": arguments['phoneCode'],
      "code": otpNumber,
    };

    var emailBody = {
      "email": arguments['email'],
      "code": otpNumber,
    };

    ApiRequest.post(
        uri: API.verifyOtp,
        postBody: arguments['medium'] == 'phone' ? phoneBody : emailBody,
        successAction: (response) => Navigator.pushNamed(
                context, Routes.loginResetConfirmation,
                arguments: {
                  'data': response,
                }));
  }

  static resendOtp(medium, context) {
    medium == 'phone'
        ? Provider.of<PhoneModel>(context, listen: false).checkAccount(context)
        : Provider.of<EmailModel>(context, listen: false).checkAccount(context);
  }
}
