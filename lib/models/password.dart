import 'package:flutter/material.dart';
import '../../api/api_request.dart';
import '../../utils/api.dart';
import '../../utils/routes.dart';
import 'alert.dart';

class PasswordModel extends ChangeNotifier {
  late TextEditingController _passwordController;
  late TextEditingController _newPasswordController;
  late TextEditingController _confirmPasswordController;

  TextEditingController get passwordController => _passwordController;
  TextEditingController get newPasswordController => _newPasswordController;
  TextEditingController get confirmPasswordController => _confirmPasswordController;

  void initPasswordController() {
    _passwordController = TextEditingController(text: '');
    _confirmPasswordController = TextEditingController(text: '');
  }

  void initResetPasswordController() {
    _newPasswordController = TextEditingController(text: '');
    _confirmPasswordController = TextEditingController(text: '');
  }

  void disposePasswordController() {
    _passwordController.dispose();
    _confirmPasswordController.dispose();
  }

    void disposeResetPasswordController() {
    _newPasswordController.dispose();
    _confirmPasswordController.dispose();
  }

  void updatePlatform() {
    notifyListeners();
  }

  void updateFormValue(controller, newValue) {
    switch (controller) {
      case 'password':
        _passwordController = TextEditingController(text: newValue);
        break;
      default:
        break;
    }
  }

    resetPassword(context, token) async {
    try {
      await ApiRequest.post(
          // port: '3004',
          uri: API.resetPassword,
          postBody: {
            "resetPasswordToken": token,
            'password': newPasswordController.text,
            'passwordConfirmation': confirmPasswordController.text,
          },
          successAction: (_) => Navigator.pushNamed(context, Routes.home));
    } catch (e) {
      Alert().showMyDialog(context, e);
    }
  }
}
