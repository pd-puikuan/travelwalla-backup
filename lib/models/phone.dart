import 'package:flutter/material.dart';

import 'package:country_picker/country_picker.dart';

import '../../../utils/strings.dart';

import '../../../widgets/forms/text_input.dart';

import '../../api/api_request.dart';
import '../../utils/api.dart';

import 'package:provider/provider.dart';
import '../../utils/routes.dart';

import 'password.dart';
import 'alert.dart';

class PhoneModel extends ChangeNotifier {
  late TextEditingController _phoneCodeController,
      _phoneNumController,
      _passwordController;

  String _loginMedium = 'phonePw';
  String _prevLoginMedium = 'email';

  TextEditingController get phoneCodeController => _phoneCodeController;
  TextEditingController get phoneNumController => _phoneNumController;
  TextEditingController get passwordController => _passwordController;
  String get loginMedium => _loginMedium;
  String get prevLoginMedium => _prevLoginMedium;

  updateLoginMedium(medium, prev) {
    _loginMedium = medium;
    _prevLoginMedium = prev;
    notifyListeners();
  }

  void initPhoneLoginController() {
    _phoneCodeController = TextEditingController(text: '+60');
    _phoneNumController = TextEditingController(text: '');
    _passwordController = TextEditingController(text: '');
  }

  void disposePhoneLoginController() {
    _phoneCodeController.dispose();
    _phoneNumController.dispose();
    _passwordController.dispose();
  }

  void updateFormValue(controller, newValue) {
    switch (controller) {
      case 'phoneCode':
        _phoneCodeController = TextEditingController(text: newValue);
        break;
      case 'phoneNum':
        _phoneNumController = TextEditingController(text: newValue);
        break;
      case 'password':
        _passwordController = TextEditingController(text: newValue);
        break;
      default:
        break;
    }
    notifyListeners();
  }

  directToOtp(context) {
    Navigator.pushNamed(context, '/otp', arguments: {
      'phoneNumber': phoneNumController.text,
      'phoneCode': phoneCodeController.text.substring(1),
      'password': passwordController.text,
      'medium': 'phone',
    });
  }

  loginAccount(context) async {
    var passwordController =
        Provider.of<PasswordModel>(context, listen: false).passwordController;
    try {
      await ApiRequest.post(
          // port: '3004',
          uri: API.login,
          postBody: {
            "identifier": phoneNumController.text,
            "phoneCode": phoneCodeController.text.substring(1),
            'password': passwordController.text,
          },
          successAction: (_) => Navigator.pushNamed(context, Routes.home));
    } catch (e) {
      Alert().showMyDialog(context, e);
    }
  }

  checkAccount(context) async {
    try {
      await ApiRequest.post(
          // port: '3004',
          uri: API.checkExistingAccount,
          postBody: {
            "phoneNumber": phoneNumController.text,
            "phoneCode": phoneCodeController.text.substring(1),
          },
          successAction: (_) => registerAccount(context));
    } catch (e) {
      Alert().showMyDialog(context, e);
    }
  }

  registerAccount(context) async {
    var passwordController =
        Provider.of<PasswordModel>(context, listen: false).passwordController;
    try {
      await ApiRequest.post(
          uri: API.registerUser,
          postBody: {
            "phoneNumber": phoneNumController.text,
            "phoneCode": phoneCodeController.text.substring(1),
            'password': passwordController.text,
          },
          successAction: (_) =>
              Navigator.pushNamed(context, Routes.loginSignupSuccess));
    } catch (e) {
      Alert().showMyDialog(context, e);
    }
  }

  forgotPassword(context) async {
    try {
      await ApiRequest.post(
          // port: '3004',
          uri: API.forgotPassword,
          postBody: {
            "channel": "sms",
            "phoneNumber": phoneNumController.text,
            "phoneCode": phoneCodeController.text.substring(1),
          },
          successAction: (_) =>
              Navigator.pushNamed(context, Routes.loginOtp, arguments: {
                'phoneNumber': phoneNumController.text,
                'phoneCode': phoneCodeController.text.substring(1),
                'medium': 'phone',
              }));
    } catch (e) {
      Alert().showMyDialog(context, e);
    }
  }

  generatePhoneFields(context, action) {
    return Flex(
      direction: Axis.horizontal,
      children: [
        Expanded(
          flex: 3,
          child: TextInput(
              controller: phoneCodeController,
              readOnly: true,
              enablePrefix: true,
              prefix: const Icon(Icons.language_outlined),
              label: '',
              action: () => showCountryPicker(
                    context: context,
                    showPhoneCode: true,
                    onSelect: (Country country) => action(country),
                  )),
        ),
        const SizedBox(width: 15),
        Expanded(
          flex: 5,
          child: TextInput(
            controller: phoneNumController,
            enablePrefix: true,
            prefix: const Icon(Icons.phone_android),
            label: Strings.formPhoneNumber,
          ),
        ),
      ],
    );
  }
}
