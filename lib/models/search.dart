import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../utils/strings.dart';

class SearchModel extends ChangeNotifier {
  late TextEditingController _goingToController;
  late TextEditingController _datesController;
  late TextEditingController _travellersController;
  late GlobalKey<FormState> _formKey;
  dynamic hotelDateStart = DateTime.now;
  dynamic hotelDateEnd = DateTime.now;
  int _roomCount = 1;
  int _adultCount = 0;
  int _childrenCount = 0;
  int _infantCount = 0;

  TextEditingController get goingToController => _goingToController;
  TextEditingController get datesController => _datesController;
  TextEditingController get travellersController => _travellersController;
  GlobalKey<FormState> get formKey => _formKey;
  int get roomCount => _roomCount;
  int get adultCount => _adultCount;
  int get childrenCount => _childrenCount;
  int get infantCount => _infantCount;

  void initHotelSearchController() {
    _goingToController = TextEditingController();
    _datesController = TextEditingController();
    _travellersController = TextEditingController();
    _formKey = GlobalKey<FormState>();
  }

  void disposeHotelSearchController() {
    _goingToController.dispose();
    // _datesController.dispose();
    _travellersController.dispose();
  }

  void updateSelectedDates(start, end) {
    var startDate = DateFormat('d MMM').format(start);
    var endDate = DateFormat('d MMM').format(end);
    hotelDateStart = DateFormat('y-MM-dd').format(start);
    hotelDateEnd = DateFormat('y-MM-dd').format(end);
    _datesController.text = '$startDate - $endDate';
    notifyListeners();
  }

  updateAmount(key, int amount) {
    switch (key) {
      case 'room':
        if (_roomCount <= 1 && amount < 0) break;
        _roomCount += amount;
        break;
      case 'adult':
        if (_adultCount <= 0 && amount < 0) break;
        _adultCount += amount;
        break;
      case 'children':
        if (_childrenCount <= 0 && amount < 0) break;
        _childrenCount += amount;
        break;
      case 'infant':
        if (_infantCount <= 0 && amount < 0) break;
        _infantCount += amount;
        break;
      default:
        break;
    }
    notifyListeners();
  }

  updateSelectedAmount(context) {
    var roomText = '$_roomCount room';
    var adultText = '$_adultCount adults';
    var childrenText = '$_childrenCount children';
    var infantText = '$_infantCount infant';
    _travellersController.text =
        '$roomText, $adultText, $childrenText, $infantText';
    Navigator.pop(context);
  }

  List<String> _searchHistoryList = [];

  List<String> get searchHistoryList => _searchHistoryList;

  loadHistory() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var oldList = prefs.getStringList('items');
    _searchHistoryList = oldList!.reversed.toList();
    notifyListeners();
  }

  void updateHistory(value, context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    goingToController.text = value;
    List<String>? itemValue = prefs.getStringList('items');
    await prefs.setStringList('items', <String>[...?itemValue, value]);
    loadHistory();
    Navigator.of(context)
      ..pop()
      ..pop();
  }

  generateInitialView(context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(children: [
          const Icon(Icons.flag_circle_outlined),
          const SizedBox(width: 10),
          Text(Strings.searchNearMe)
        ]),
        const SizedBox(height: 20),
        Text(Strings.searchRecent,
            style: Theme.of(context).textTheme.headline6),
        SizedBox(
            height: 300,
            child: Consumer<SearchModel>(
              builder: (context, searchModel, child) {
                return ListView.builder(
                    itemCount: searchModel.searchHistoryList.length,
                    itemBuilder: (BuildContext context, int index) {
                      return SizedBox(
                        height: 40,
                        child: Row(
                          children: [
                            const Icon(Icons.undo),
                            const SizedBox(width: 10),
                            Flexible(
                              child: Text(
                                searchModel.searchHistoryList[index],
                                overflow: TextOverflow.ellipsis,
                                maxLines: 1,
                              ),
                            )
                          ],
                        ),
                      );
                    });
              },
            )),
      ],
    );
  }
}
