import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart' as http;
import 'alert.dart';

class SocialModel extends ChangeNotifier {
  late Map<String, dynamic> _userData;

  Map<String, dynamic> get userData => _userData;

  final GoogleSignIn _googleSignIn = GoogleSignIn(
    scopes: [
      'email',
      'https://www.googleapis.com/auth/contacts.readonly',
    ],
  );

  Future<void> googleLogin(context) async {
    try {
      await _googleSignIn.signIn();
    } catch (e) {
      Alert().showMyDialog(context, e);
    }
  }

  Future<void> googleLogout() => _googleSignIn.disconnect();

  facebookLogin() async {
    var loginExist = await facebookAuth();

    if (loginExist == null) {
      final LoginResult result = await FacebookAuth.instance
          .login(); // by default we request the email and the public profile
      if (result.status == LoginStatus.success) {
        // you are logged
        _userData = await facebookLoadUser();
        final AccessToken accessToken = result.accessToken!;
      } else {
        // print(result.status);
        // print(result.message);
      }
    }
  }

  Future<void> _googleLoadUser(GoogleSignInAccount user) async {
    final http.Response response = await http.get(
      Uri.parse('https://people.googleapis.com/v1/people/me/connections'
          '?requestMask.includeField=person.names'),
      headers: await user.authHeaders,
    );
    if (response.statusCode != 200) {
      print('People API ${response.statusCode} response: ${response.body}');
      return;
    }
    final Map<String, dynamic> data =
        json.decode(response.body) as Map<String, dynamic>;
    print('GOOGLE DATA $data');
  }

  facebookLoadUser() async {
    return await FacebookAuth.instance.getUserData();
  }

  facebookAuth() async {
    final AccessToken? accessToken = await FacebookAuth.instance.accessToken;
// or FacebookAuth.i.accessToken
    if (accessToken != null) {
      // user is logged
      _userData = await facebookLoadUser();
      print(_userData);
    } else {
      return null;
    }
  }

  facebookLogout() async {
    await FacebookAuth.instance.logOut();
  }
}
