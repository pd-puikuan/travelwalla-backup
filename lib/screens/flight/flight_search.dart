import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../models/search.dart';
import '../../models/hotel.dart';

import '../../utils/strings.dart';
import '../../utils/size_config.dart';
import '../../utils/routes.dart';
import '../../utils/images.dart';

import '../../widgets/main_scaffold.dart';
import '../../widgets/forms/text_input.dart';
import '../../widgets/forms/icon_option.dart';
import '../../widgets/buttons/primary_button.dart';

class FlightSearchScreen extends StatefulWidget {
  const FlightSearchScreen({Key? key}) : super(key: key);

  @override
  State<FlightSearchScreen> createState() => _FlightSearchScreenState();
}

class _FlightSearchScreenState extends State<FlightSearchScreen> {
  @override
  void initState() {
    super.initState();
    Provider.of<SearchModel>(context, listen: false)
        .initHotelSearchController();
  }

  @override
  void dispose() {
    Provider.of<SearchModel>(context, listen: false)
        .disposeHotelSearchController();
    super.dispose();
  }

  _carousel() {
    return Container(
      width: 160,
      margin: EdgeInsets.only(right: SizeConfig.mediumMargin),
      clipBehavior: Clip.hardEdge,
      decoration: BoxDecoration(
        color: Colors.purple,
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Stack(
        fit: StackFit.expand,
        alignment: Alignment.bottomLeft,
        children: [
          Image.asset(Images.hotelImage2, fit: BoxFit.cover),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.end,
            children: const [
              Chip(label: Text('Melaka')),
              Text('Malaysia'),
            ],
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var searchModel = Provider.of<SearchModel>(context, listen: true);

    return MainScaffold(
        title: Strings.appbarSearchFlight,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(Strings.flightSearchHeader,
                style: TextStyle(
                  fontSize: SizeConfig.subtitleFontSize,
                  fontWeight: FontWeight.bold,
                )),
            Form(
              key: searchModel.formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Flex(
                    direction: Axis.horizontal,
                    children: [
                      IconOption(Strings.formOneWay, Icons.bed),
                      const SizedBox(width: 15),
                      IconOption(
                          Strings.formRoundTrip, Icons.king_bed_outlined),
                    ],
                  ),
                  TextInput(
                      controller: searchModel.goingToController,
                      enablePrefix: true,
                      prefix: const Icon(Icons.pin_drop_outlined),
                      label: Strings.formFlightFrom,
                      action: () =>
                          Navigator.pushNamed(context, Routes.searchLocation)),
                  TextInput(
                      controller: searchModel.goingToController,
                      enablePrefix: true,
                      prefix: const Icon(Icons.pin_drop_outlined),
                      label: Strings.formFlightTo,
                      action: () =>
                          Navigator.pushNamed(context, Routes.searchLocation)),
                  TextInput(
                      controller: searchModel.datesController,
                      enablePrefix: true,
                      readOnly: true,
                      prefix: const Icon(Icons.calendar_month_outlined),
                      label: Strings.formFlightDate,
                      action: () =>
                          Navigator.pushNamed(context, Routes.searchDates)),
                  TextInput(
                      controller: searchModel.travellersController,
                      enablePrefix: true,
                      readOnly: true,
                      prefix: const Icon(Icons.supervisor_account_outlined),
                      label: Strings.formFlightPassengers,
                      action: () =>
                          Navigator.pushNamed(context, Routes.searchRooms)),
                  Text(Strings.flightSeatType,
                      style: TextStyle(
                        fontSize: SizeConfig.subtitleFontSize,
                        fontWeight: FontWeight.bold,
                      )),
                  Flex(
                    direction: Axis.horizontal,
                    children: [
                      IconOption(Strings.formSeatEconomy, Icons.bed),
                      const SizedBox(width: 15),
                      IconOption(
                          Strings.formSeatBusiness, Icons.king_bed_outlined),
                      const SizedBox(width: 15),
                      IconOption(
                          Strings.formSeatFirstClass, Icons.king_bed_outlined),
                    ],
                  ),
                  PrimaryButton(
                      Strings.searchButton,
                      () async =>
                          await Provider.of<HotelModel>(context, listen: false)
                              .getHotelList(context)),
                  const SizedBox(height: 20),
                  Text(Strings.flightFlightDeals,
                      style: const TextStyle(fontWeight: FontWeight.bold)),
                  const SizedBox(height: 10),
                  SizedBox(
                    height: 250,
                    child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: 5,
                        itemBuilder: (context, index) {
                          return _carousel();
                        }),
                  )
                ],
              ),
            ),
          ],
        ));
  }
}
