import 'package:flutter/material.dart';

import '../../utils/size_config.dart';
import '../../utils/strings.dart';

import '../../widgets/main_scaffold.dart';

import 'home_header.dart';
import 'home_category.dart';
import 'home_offers.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return MainScaffold(
          child: SizedBox(
            height: MediaQuery.of(context).size.height,
            child: Flex(
                direction: Axis.vertical,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: SizeConfig.safeBlockVertical * 2.5),
                  const HomeHeader(),
                  SizedBox(height: SizeConfig.safeBlockVertical * 2.5),
                  Text(Strings.homeCategoryHeader,
                      style: TextStyle(
                          fontSize: SizeConfig.mediumFontSize,
                          fontWeight: FontWeight.bold)),
                  const HomeCategory(),
                  SizedBox(height: SizeConfig.safeBlockVertical * 2.5),
                  Text(Strings.homeOfferHeader,
                      style: TextStyle(
                          fontSize: SizeConfig.mediumFontSize,
                          fontWeight: FontWeight.bold)),
                  const HomeOffers(),
                ]),
          ),
        );
  }
}
