import 'package:flutter/material.dart';

import '../../utils/strings.dart';
import '../../utils/routes.dart';

import '../../widgets/buttons/home_button.dart';

class HomeCategory extends StatelessWidget {
  const HomeCategory({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 2,
      child: Flex(
        direction: Axis.horizontal,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                HomeButton(Strings.homeHotelButton, Routes.hotelSearch, 'left'),
                HomeButton(Strings.homeFlightButton, Routes.flightSearch, 'left'),
              ],
            ),
          ),
          HomeButton(Strings.homeJourneyButton, Routes.hotelDetails, 'right'),
        ],
      ),
    );
  }
}
