import 'package:flutter/material.dart';

import '../../widgets/buttons/tertiary_button.dart';

import '../../utils/size_config.dart';
import '../../utils/strings.dart';
import '../../utils/routes.dart';

class HomeHeader extends StatelessWidget {
  const HomeHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            Strings.homeHeader,
            style: TextStyle(fontSize: SizeConfig.largeFontSize),
          ),
          Text(
            Strings.homeText,
            style: TextStyle(fontSize: SizeConfig.mediumFontSize),
          ),
          SizedBox(height: SizeConfig.safeBlockVertical * 2.5),
          Flex(
            direction: Axis.horizontal,
            children: [
              Expanded(
                  flex: 1,
                  child: TertiaryButton(Strings.homeProfileButton, Routes.loginSignin)),
              const Expanded(flex: 1, child: SizedBox()),
            ],
          ),
        ],
      ),
    );
  }
}
