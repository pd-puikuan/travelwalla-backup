import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

import '../../utils/size_config.dart';

class HomeOffers extends StatelessWidget {
  const HomeOffers({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
        flex: 1,
        child: CarouselSlider.builder(
          options: CarouselOptions(
            initialPage: 1,
            height: SizeConfig.safeBlockVertical * 30,
            autoPlay: false,
            viewportFraction: 0.7,
            aspectRatio: 3 / 4,
          ),
          itemCount: 5,
          itemBuilder:
              (BuildContext context, int itemIndex, int pageViewIndex) =>
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.symmetric(vertical: SizeConfig.mediumMargin, horizontal: SizeConfig.smallMargin),
                      color: Colors.pink,
                      height: 20,
                      child: Text(itemIndex.toString())),
        ));
  }
}
