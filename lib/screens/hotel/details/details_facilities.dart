import 'package:flutter/material.dart';

import '../../../utils/strings.dart';
import '../../../utils/size_config.dart';

import '../../../widgets/main_scaffold.dart';

class DetailsFacilitiesScreen extends StatelessWidget {
  DetailsFacilitiesScreen({Key? key}) : super(key: key);

  final _internet = {
    'label': 'Internet',
    'icon': Icons.wifi,
    'details': [
      'Available in all rooms: Free WiFi',
      'Available in some public areas: Free WiFi'
    ],
  };

  final _fnb = {
    'label': 'Food and drink',
    'icon': Icons.fastfood_outlined,
    'details': ['1 bar', 'Coffee/tea in common area(s)'],
  };

  final _conveniences = {
    'label': 'Conveniences',
    'icon': Icons.cases_outlined,
    'details': [
      'Elevator',
      'Free newspapers in lobby',
      'Front-desk safe',
      'Library',
      'Vending machine',
      'Water dispenser'
    ],
  };

  final _guestServices = {
    'label': 'Guest Services',
    'icon': Icons.supervisor_account_rounded,
    'details': [
      '24-hour front desk',
      'Daily housekeeping',
      'Dry cleaning service',
      'Laundry facilities',
      'Luggage storage',
      'Tour / ticket assistance'
    ],
  };

  final _languages = {
    'label': 'Languages',
    'icon': Icons.language,
    'details': ['English', 'Mandarin'],
  };

  _section(title, items, context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(title,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Theme.of(context).colorScheme.secondary)),
        const SizedBox(height: 10),
        ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: items.length,
            itemBuilder: (context, index) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  RichText(
                    text: TextSpan(children: [
                      WidgetSpan(
                          child: Container(
                        margin: EdgeInsets.only(right: SizeConfig.smallMargin),
                        child: Icon(items[index]['icon'],
                            size: SizeConfig.mediumFontSize),
                      )),
                      TextSpan(
                          text: items[index]['label'],
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              height: SizeConfig.textHeight))
                    ]),
                  ),
                  ListView.builder(
                      itemCount: items[index]['details'].length,
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemBuilder: (context, subIndex) {
                        return Text(items[index]['details'][subIndex],
                            style: TextStyle(height: SizeConfig.textHeight));
                      }),
                  const SizedBox(height: 15),
                ],
              );
            }),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var facilities = [_internet, _fnb, _conveniences, _guestServices, _languages];
    return MainScaffold(
      title: Strings.appbarHotelFacilities,
      child: Column(
        children: [
          _section('Property Facilities', facilities, context),
          const Divider(),
          _section('Room Amenities', facilities, context),
        ],
      ),
    );
  }
}
