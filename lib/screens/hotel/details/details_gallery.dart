import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import '../../../utils/strings.dart';
import '../../../utils/size_config.dart';

class DetailsGalleryScreen extends StatelessWidget {
  DetailsGalleryScreen({Key? key}) : super(key: key);

  final imageSet = [];
  final imageLength = 15;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    for (var i = 1; i <= 15; i++) {
      var string = '$i.jpeg';
      imageSet.add(string);
    }
    return Scaffold(
      appBar: AppBar(
        title: Text(
          Strings.appbarHotelGallery,
          style: TextStyle(fontSize: SizeConfig.mediumFontSize),
        ),
      ),
      body: Container(
        margin: SizeConfig.safeContainerMargin,
        child: StaggeredGridView.countBuilder(
          staggeredTileBuilder: (index) => index % 2 == 0
              ? const StaggeredTile.count(1, 2)
              : const StaggeredTile.count(1, 1),
          mainAxisSpacing: 10,
          crossAxisSpacing: 10,
          crossAxisCount: 2,
          itemCount: imageLength,
          itemBuilder: (context, index) => buildImageCard(index),
        ),
      ),
    );
  }

  Widget buildImageCard(int index) => ClipRRect(
      borderRadius: BorderRadius.circular(8),
      child: Image.asset('assets/images/hotel-images/${imageSet[index]}',
          fit: BoxFit.cover));
}
