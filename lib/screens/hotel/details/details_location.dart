import 'package:flutter/material.dart';

import '../../../widgets/main_scaffold.dart';

import '../../../utils/strings.dart';
import '../../../utils/size_config.dart';

class DetailsLocationScreen extends StatelessWidget {
  const DetailsLocationScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return MainScaffold(
        title: Strings.appbarHotelLocation,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(bottom: SizeConfig.mediumMargin),
              child: Text(
                'The Westin Langkawi Resort & Spa unveils comfort. Live your best in our newly rejuvenated rooms. Reflecting a modern contemporary resort feel, these rooms\' refreshing decor welcomes you to relax and rejuvenate.',
                style: TextStyle(
                  height: SizeConfig.textHeight,
                ),
              ),
            ),
            Container(
                margin: EdgeInsets.symmetric(vertical: SizeConfig.mediumMargin),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'What\'s Nearby',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Theme.of(context).colorScheme.secondary,
                          height: SizeConfig.textHeight,
                        ),
                      ),
                      Text(
                        'Private beachfront - 4 min walk',
                        style: TextStyle(
                          height: SizeConfig.textHeight,
                        ),
                      ),
                      Text(
                        'Main harbor of Langkawi - 6 min walk',
                        style: TextStyle(
                          height: SizeConfig.textHeight,
                        ),
                      ),
                      Text(
                        'Kuah duty-free shopping centers - 10 min walk',
                        style: TextStyle(
                          height: SizeConfig.textHeight,
                        ),
                      ),
                    ])),
            const Divider(),
            Container(
                margin: EdgeInsets.symmetric(vertical: SizeConfig.mediumMargin),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Restaurants',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Theme.of(context).colorScheme.secondary,
                          height: SizeConfig.textHeight,
                        ),
                      ),
                      Text(
                        'Double Barrel Coffee - 2 min walk',
                        style: TextStyle(
                          height: SizeConfig.textHeight,
                        ),
                      ),
                      Text(
                        'Starbucks - 2 min walk',
                        style: TextStyle(
                          height: SizeConfig.textHeight,
                        ),
                      ),
                      Text(
                        'Goshu Ramen Tei - 3min walk',
                        style: TextStyle(
                          height: SizeConfig.textHeight,
                        ),
                      ),
                    ])),
          ],
        ));
  }
}
