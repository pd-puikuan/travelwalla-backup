import 'package:flutter/material.dart';

import '../../../utils/strings.dart';
import '../../../utils/size_config.dart';

import '../../../widgets/main_scaffold.dart';
import '../../../widgets/chips/filter_chip.dart';

class DetailsReviewsScreen extends StatelessWidget {
  const DetailsReviewsScreen({Key? key}) : super(key: key);

  _reviews() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Text('Sam Smith', style: TextStyle(fontWeight: FontWeight.bold)),
        Flex(
          direction: Axis.horizontal,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: FittedBox(
                fit: BoxFit.fitWidth,
                child: RichText(
                  text: TextSpan(children: [
                    WidgetSpan(
                        child: Icon(Icons.star_rounded,
                            size: SizeConfig.mediumFontSize, color: Colors.amber)),
                    WidgetSpan(
                        child: Icon(Icons.star_rounded,
                            size: SizeConfig.mediumFontSize, color: Colors.amber)),
                    WidgetSpan(
                        child: Icon(Icons.star_rounded,
                            size: SizeConfig.mediumFontSize, color: Colors.amber)),
                    WidgetSpan(
                        child: Icon(Icons.star_rounded,
                            size: SizeConfig.mediumFontSize, color: Colors.amber)),
                    WidgetSpan(
                        child: Icon(Icons.star_rounded,
                            size: SizeConfig.mediumFontSize, color: Colors.grey)),
                    TextSpan(
                      text: '(120 reviews)',
                      style: TextStyle(
                          color: Colors.black, height: SizeConfig.textHeight),
                    ),
                  ]),
                ),
              ),
            ),
            Flexible(
              child: Align(
                alignment: Alignment.centerRight,
                child: FittedBox(
                  fit: BoxFit.fitWidth,
                  child: RichText(
                    text: TextSpan(children: [
                      WidgetSpan(
                          child: Container(
                              margin:
                                  EdgeInsets.only(right: SizeConfig.xSmallMargin),
                              child: Icon(Icons.language,
                                  size: SizeConfig.mediumFontSize))),
                      TextSpan(
                          text: 'Singapore',
                          style: TextStyle(
                              color: Colors.black,
                              height: SizeConfig.textHeight)),
                    ]),
                  ),
                ),
              ),
            ),
          ],
        ),
        const Text(
            'Cleanliness, staff & service, property conditions & facilities, room comfort'),
        const SizedBox(height: 20),
        const Divider(),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return MainScaffold(
      title: Strings.appbarHotelReviews,
      child: Column(
        children: [
          Flex(
            direction: Axis.horizontal,
            children: [
              Container(
                padding: EdgeInsets.symmetric(
                    horizontal: SizeConfig.smallPadding * 1.5),
                decoration: BoxDecoration(
                  border: Border.all(
                      color: Theme.of(context).colorScheme.secondary),
                  borderRadius: BorderRadius.circular(5.0),
                ),
                child: Text('4.8',
                    style: TextStyle(
                        fontSize: SizeConfig.headerFontSize * 0.9,
                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).colorScheme.secondary)),
              ),
              const SizedBox(width: 8),
              Expanded(
                  child: Text('Impressive',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Theme.of(context).colorScheme.secondary))),
            ],
          ),
          const SizedBox(height: 20),
          FilterChipSet(const [
            '5 - Excellent',
            '4 - Good',
            '3 - Okay',
            '2 - Poor',
            '1 - Terrible'
          ]),
          const SizedBox(height: 20),
          _reviews(),
          _reviews(),
          _reviews(),
          _reviews(),
          _reviews(),
          _reviews(),
        ],
      ),
    );
  }
}
