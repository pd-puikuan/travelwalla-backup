import 'package:flutter/material.dart';

import '../../../utils/images.dart';
import '../../../utils/strings.dart';
import '../../../utils/size_config.dart';

import '../../../widgets/cards/booking_card.dart';
import '../../../widgets/main_scaffold.dart';

class DetailsRoomScreen extends StatelessWidget {
  DetailsRoomScreen({Key? key}) : super(key: key);

  final _bedroom = {
    'label': 'Bedroom',
    'icon': Icons.bed,
    'details': [
      'Bed sheets',
      'Blackout drapes/curtains',
      'Hypo-allergenic bedding',
      'Pillow-top mattress',
      'Premium bedding',
    ],
  };

  final _entertainment = {
    'label': 'Entertainment',
    'icon': Icons.tv,
    'details': [
      'Netflix',
      'Premium channels',
    ],
  };

  final _fnb = {
    'label': 'Food and drink',
    'icon': Icons.fastfood_outlined,
    'details': ['Coffee/tea maker', 'Electric kettle', 'Free bottled water'],
  };

  _facilities(label, icon, context) {
    return RichText(
      text: TextSpan(children: [
        WidgetSpan(
            child: Container(
          margin: EdgeInsets.only(right: SizeConfig.smallMargin),
          child: Icon(icon, size: SizeConfig.mediumFontSize),
        )),
        TextSpan(
            text: label,
            style:
                TextStyle(color: Colors.black, height: SizeConfig.textHeight))
      ]),
    );
  }

  _section(title, items, context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(title,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Theme.of(context).colorScheme.secondary)),
        const SizedBox(height: 10),
        ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: items.length,
            itemBuilder: (context, index) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  RichText(
                    text: TextSpan(children: [
                      WidgetSpan(
                          child: Container(
                        margin: EdgeInsets.only(right: SizeConfig.smallMargin),
                        child: Icon(items[index]['icon'],
                            size: SizeConfig.mediumFontSize),
                      )),
                      TextSpan(
                          text: items[index]['label'],
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              height: SizeConfig.textHeight))
                    ]),
                  ),
                  ListView.builder(
                      itemCount: items[index]['details'].length,
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemBuilder: (context, subIndex) {
                        return Text(items[index]['details'][subIndex],
                            style: TextStyle(height: SizeConfig.textHeight));
                      }),
                  const SizedBox(height: 15),
                ],
              );
            }),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var amenities = [_bedroom, _entertainment, _fnb];

    return MainScaffold(
      title: Strings.appbarHotelRooms,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          BookingCard(
            image: Images.hotelImage3,
            title: 'The Westin Langkawi Resort & Spa',
            subtitle: 'Guest Room',
            desc: '2 Adults, sea view',
          ),
          const SizedBox(height: 20),
          Text(Strings.hotelFacilities,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Theme.of(context).colorScheme.secondary)),
          _facilities('2 Queen Beds', Icons.bed, context),
          _facilities('Free WiFi', Icons.wifi, context),
          _facilities('Free breakfast', Icons.fastfood_outlined, context),
          _facilities('Air conditioning', Icons.air, context),
          _facilities('Laundry', Icons.local_laundry_service_outlined, context),
          _facilities('Free Parking', Icons.car_repair, context),
          const SizedBox(height: 10),
          const Divider(),
          const SizedBox(height: 10),
          Text(Strings.bookingCancellationHeader,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Theme.of(context).colorScheme.secondary)),
          const SizedBox(height: 10),
          const Divider(),
          const SizedBox(height: 10),
          _section('Room Amenities', amenities, context),
        ],
      ),
    );
  }
}
