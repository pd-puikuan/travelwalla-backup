import 'package:flutter/material.dart';
import 'package:travelwalla_mobile/widgets/buttons/primary_button.dart';

import '../../utils/strings.dart';
import '../../utils/images.dart';
import '../../utils/routes.dart';
import '../../utils/size_config.dart';

import '../../widgets/main_scaffold.dart';
import '../../widgets/cards/booking_card.dart';
import '../../widgets/chips/filter_chip.dart';
import '../../widgets/forms/text_input.dart';
import '../../widgets/forms/icon_option.dart';
import '../../widgets/snippets/snippet_with_subheader.dart';
import '../../widgets/footer/price_footer.dart';

class HotelBookingScreen extends StatelessWidget {
  const HotelBookingScreen({Key? key}) : super(key: key);

  _header(title, context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 10),
        const Divider(),
        Text(
          title,
          style: TextStyle(
            fontSize: SizeConfig.subtitleFontSize,
            fontWeight: FontWeight.bold,
            color: Theme.of(context).colorScheme.secondary,
            height: SizeConfig.textHeight,
          ),
        ),
        const SizedBox(height: 10),
      ],
    );
  }

  _contactForm() {
    return Column(
      children: [
        TextInput(
            controller: TextEditingController(), label: Strings.formFullName),
        Flex(
          direction: Axis.horizontal,
          children: [
            Expanded(
              flex: 4,
              child: TextInput(
                  controller: TextEditingController(),
                  label: Strings.formRegionCode),
            ),
            const SizedBox(width: 15),
            Expanded(
              flex: 5,
              child: TextInput(
                  controller: TextEditingController(),
                  label: Strings.formPhoneNumber),
            ),
          ],
        ),
        TextInput(
            controller: TextEditingController(), label: Strings.formCountry),
      ],
    );
  }

  _checkBox(text) {
    return Flex(direction: Axis.horizontal, children: [
      Checkbox(
        value: true,
        shape: const CircleBorder(),
        onChanged: (bool? value) {},
      ),
      Text(text),
    ]);
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var footerHeight = 10;

    return MainScaffold(
      title: Strings.appbarBookingDetails,
      footer: PriceFooter(
          price: 1669.00, footerHeight: footerHeight, isSummary: false),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          BookingCard(
              image: Images.paymentPending,
              title: 'The Westin Langkawi Resort & Spa',
              desc: '25 Jun - 29 Jun (5 Nights)',
              offer: 'Free Cancellation | until 20 Jun 2022'),
          SnippetWithSubheader(
              title: Strings.bookingRoomHeader,
              subtitle: 'Guest Room',
              desc: '2 Adults, sea view',
              action: () => Navigator.pushNamed(context, Routes.detailsRoom)),
          _header(Strings.bookingContactHeader, context),
          _contactForm(),
          _checkBox(Strings.bookingForAnotherPerson),
          _contactForm(),
          _header(Strings.bookingAddonHeader, context),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(Strings.bookingAddonBed,
                  style: const TextStyle(fontWeight: FontWeight.bold)),
              Flex(
                direction: Axis.horizontal,
                children: const [
                  IconOption('Large Bed', Icons.bed),
                  SizedBox(width: 15),
                  IconOption('Twin Beds', Icons.king_bed_outlined),
                ],
              ),
              const SizedBox(height: 10),
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(Strings.bookingAddonSmoking,
                  style: const TextStyle(fontWeight: FontWeight.bold)),
              Flex(
                direction: Axis.horizontal,
                children: const [
                  IconOption('Non-smoking Room', Icons.smoke_free_outlined),
                  SizedBox(width: 15),
                  IconOption('Smoking Room', Icons.smoking_rooms_outlined),
                ],
              ),
              const SizedBox(height: 10),
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(Strings.bookingAddonOthers,
                  style: const TextStyle(fontWeight: FontWeight.bold)),
              FilterChipSet(const [
                'Connecting Rooms',
                'High Floor',
                'Check-in time',
                'Check-out time'
              ]),
              const SizedBox(height: 10),
            ],
          ),
          TextFormField(
            keyboardType: TextInputType.multiline,
            maxLines: null,
            decoration: InputDecoration(
              contentPadding: const EdgeInsets.all(20.0),
              border: const OutlineInputBorder(),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(width: 1, color: Colors.grey.shade400),
              ),
              label: Text(Strings.formRemarks),
            ),
          ),
          _header(Strings.bookingCancellationHeader, context),
          _checkBox(Strings.bookingCancellationOption1),
          _checkBox(Strings.bookingCancellationOption2),
          _header(Strings.bookingInfoHeader, context),
          _header(Strings.bookingPaymentHeader, context),
          Column(
            children: [
              Flex(
                direction: Axis.horizontal,
                children: [
                  IconOption(Strings.bookingPaymentOptions[0],
                      Icons.credit_card_outlined),
                  const SizedBox(width: 15),
                  IconOption(Strings.bookingPaymentOptions[1],
                      Icons.home_work_outlined),
                ],
              ),
              Flex(
                direction: Axis.horizontal,
                children: [
                  IconOption(Strings.bookingPaymentOptions[2],
                      Icons.calendar_month_outlined),
                  const SizedBox(width: 15),
                  IconOption(
                      Strings.bookingPaymentOptions[3], Icons.money_outlined),
                ],
              ),
            ],
          ),
          _header(Strings.bookingSummaryHeader, context),
          TextInput(
              controller: TextEditingController(),
              label: Strings.formCouponCode),
          _checkBox('1,000 ${Strings.bookingUseRewardPoints}'),
          PrimaryButton(Strings.bookingContinueButton, () => Navigator.pushNamed(context, Routes.hotelConfirmation)),
          SizedBox(
            height: SizeConfig.safeBlockVertical * footerHeight,
          ),
        ],
      ),
    );
  }
}
