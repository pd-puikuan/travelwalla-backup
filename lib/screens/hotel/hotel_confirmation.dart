import 'package:flutter/material.dart';

import '../../utils/strings.dart';
import '../../utils/images.dart';
import '../../utils/size_config.dart';
import '../../utils/routes.dart';

import '../../widgets/main_scaffold.dart';
import '../../widgets/cards/booking_card.dart';
import '../../widgets/snippets/snippet_with_subheader.dart';
import '../../widgets/footer/price_footer.dart';

class HotelConfirmationScreen extends StatelessWidget {
  const HotelConfirmationScreen({Key? key}) : super(key: key);

  _header(title, context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 10),
        const Divider(),
        Text(
          title,
          style: TextStyle(
            fontSize: SizeConfig.subtitleFontSize,
            fontWeight: FontWeight.bold,
            color: Theme.of(context).colorScheme.secondary,
            height: SizeConfig.textHeight,
          ),
        ),
        const SizedBox(height: 10),
      ],
    );
  }

  _content(label, value, context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        label != ''
            ? Text(
                label,
                style: TextStyle(
                    fontSize: SizeConfig.smallFontSize,
                    color: Colors.grey,
                    height: SizeConfig.textHeight),
              )
            : const SizedBox.shrink(),
        Text(
          value,
          style: TextStyle(height: SizeConfig.textHeight),
        ),
        const SizedBox(height: 10),
      ],
    );
  }

  _priceBreakdown({label, amount, discount = false}) {
    var discountIndicator = '-';

    return Flex(
        direction: Axis.horizontal,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            label,
            style: TextStyle(height: SizeConfig.textHeight),
          ),
          Text(
            "${discount ? discountIndicator : ''} ${Strings.currency} ${amount.toStringAsFixed(2)}",
            style: TextStyle(
                fontWeight: FontWeight.bold, height: SizeConfig.textHeight),
          ),
        ]);
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var footerHeight = 30;

    return MainScaffold(
      title: Strings.appbarBookingReview,
      footer: PriceFooter(price: 1669.00, footerHeight: footerHeight),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          BookingCard(
              image: Images.paymentPending,
              title: 'The Westin Langkawi Resort & Spa',
              desc: '25 Jun - 29 Jun (5 Nights)',
              offer: 'Free Cancellation | until 20 Jun 2022'),
          SnippetWithSubheader(
              title: Strings.bookingRoomHeader,
              subtitle: 'Guest Room',
              desc: '2 Adults, sea view',
              action: () => Navigator.pushNamed(context, Routes.detailsRoom)),
          _header(Strings.bookingContactHeader, context),
          _content('Full Name', 'John Smith', context),
          _content('Phone Number', '+6012 1234567', context),
          _content('Country', 'Malaysia', context),
          Flex(direction: Axis.horizontal, children: [
            Checkbox(
              value: true,
              shape: const CircleBorder(),
              onChanged: (bool? value) {},
            ),
            Text(Strings.bookingForAnotherPerson),
          ]),
          _content('Full Name', 'John Doe', context),
          _content('Phone Number', '+6012 00000000', context),
          _content('Country', 'Malaysia', context),
          _header(Strings.bookingAddonHeader, context),
          _content('Bed type', 'Twin beds', context),
          _content('Smoking preferences', 'Non-smoking Room', context),
          _content('Other add-ons', 'Connecting rooms, high floor', context),
          _content(
              'Remarks',
              'Please provide the room close to the lift as having injured leg',
              context),
          _header(Strings.bookingCancellationHeader, context),
          _content('', 'Fully refundable before 20 Jun 2022', context),
          _header(Strings.bookingInfoHeader, context),
          _header(Strings.bookingPaymentHeader, context),
          _content('', 'Pay Now', context),
          _header(Strings.bookingSummaryHeader, context),
          _content('Coupon code', '24HOURSALE', context),
          _content('Reward points', '1,000 reward points', context),
          _header(Strings.bookingReviewPrice, context),
          _priceBreakdown(
            label: '1 room x 3 night',
            amount: 1635.00,
          ),
          _priceBreakdown(
            label: Strings.bookingDiscountPrice,
            amount: 10.00,
            discount: true,
          ),
          _priceBreakdown(
            label: Strings.bookingTaxes,
            amount: 54.00,
          ),
          _priceBreakdown(
            label: Strings.bookingAddOnPrice,
            amount: 30.00,
          ),
          _priceBreakdown(
            label: '1,000 ${Strings.bookingUseRewardPoints.toLowerCase()}',
            amount: 10.00,
            discount: true,
          ),
          SizedBox(
            height: SizeConfig.safeBlockVertical * footerHeight,
          ),
        ],
      ),
    );
  }
}
