import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:share_plus/share_plus.dart';
import '../../widgets/buttons/primary_button.dart';
import '../../widgets/buttons/secondary_button.dart';
import '../../widgets/chips/campaign_chip.dart';
import '../../widgets/chips/offer_chip.dart';
import '../../widgets/chips/facilities_chip.dart';
import '../../widgets/carousel/carousel_images.dart';

import '../../utils/images.dart';
import '../../utils/routes.dart';
import '../../utils/strings.dart';
import '../../utils/size_config.dart';

class HotelDetailsScreen extends StatelessWidget {
  HotelDetailsScreen({Key? key}) : super(key: key);

  _topSection() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('The Westin Langkawi Resort & Spa',
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: SizeConfig.subtitleFontSize)),
        Text('Kuah, Langkawi',
            style: TextStyle(fontSize: SizeConfig.smallFontSize)),
        RichText(
          text: TextSpan(
            children: [
              WidgetSpan(
                  child: Icon(Icons.star_rounded,
                      size: SizeConfig.mediumFontSize, color: Colors.amber)),
              WidgetSpan(
                  child: Icon(Icons.star_rounded,
                      size: SizeConfig.mediumFontSize, color: Colors.amber)),
              WidgetSpan(
                  child: Icon(Icons.star_rounded,
                      size: SizeConfig.mediumFontSize, color: Colors.amber)),
              WidgetSpan(
                  child: Icon(Icons.star_rounded,
                      size: SizeConfig.mediumFontSize, color: Colors.amber)),
              WidgetSpan(
                  child: Icon(Icons.star_rounded,
                      size: SizeConfig.mediumFontSize, color: Colors.grey)),
              TextSpan(
                text: '(120 reviews)',
                style: TextStyle(
                    color: Colors.black, height: SizeConfig.textHeight),
              ),
                            TextSpan(
                text: 'Share',
                recognizer: TapGestureRecognizer()..onTap = () {Share.share('Test');}, 
                style: TextStyle(
                    color: Colors.black, height: SizeConfig.textHeight),
              ),
            ],
          ),
        ),
        CampaignChipSet(const [
          '24HOURSALE | Coupon Applied! RM20 Off',
          '20OFFDISCOUNT',
          'FREECOUPON',
          'FREEBREAKFASTHITEA'
        ]),
      ],
    );
  }

  _tabs(label, image, route, context) {
    return Expanded(
        child: Container(
      margin: EdgeInsets.all(SizeConfig.smallMargin),
      child: GestureDetector(
        onTap: () => Navigator.pushNamed(context, route),
        child: Column(
          children: [
            Image.asset(image),
            Text(label),
          ],
        ),
      ),
    ));
  }

  _room(context) {
    var roomImage = Container(
      margin: EdgeInsets.symmetric(horizontal: SizeConfig.xSmallMargin),
      clipBehavior: Clip.hardEdge,
      height: SizeConfig.safeBlockVertical * 25,
      width: double.infinity,
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.0)),
      child: Image.asset(
        Images.hotelImage1,
        fit: BoxFit.cover,
      ),
    );

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CarouselImages([roomImage, roomImage, roomImage]),
        Text('Guest Room',
            style: TextStyle(
                fontWeight: FontWeight.bold,
                height: SizeConfig.textHeight,
                fontSize: SizeConfig.subtitleFontSize)),
        const OfferChip('Free Cancellation'),
        FacilitiesChipSet([
          {Icons.bed, '1 King Bed'},
          {Icons.space_dashboard_outlined, '183 sq ft'},
          {Icons.wifi, 'Free WiFi'},
        ]),
        GestureDetector(
          onTap: () => Navigator.pushNamed(context, Routes.detailsRoom),
          child: Text('${Strings.bookingRoomHeader} >',
              style: TextStyle(
                  fontSize: SizeConfig.smallFontSize,
                  height: SizeConfig.textHeight,
                  color: Theme.of(context).colorScheme.primary)),
        ),
        Flex(
          direction: Axis.horizontal,
          children: [
            Expanded(
                flex: 3,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    RichText(
                        text: TextSpan(children: [
                      TextSpan(
                          text: 'RM980.00',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                              fontSize: SizeConfig.subtitleFontSize)),
                      WidgetSpan(
                          child: Container(
                              margin: EdgeInsets.only(
                                  left: SizeConfig.xSmallMargin),
                              child: Text('RM1000.00',
                                  style: TextStyle(
                                      decoration: TextDecoration.lineThrough,
                                      fontSize: SizeConfig.smallFontSize,
                                      color: Colors.black)))),
                    ])),
                    Text('(${Strings.hotelIncludedTax})',
                        style: TextStyle(fontSize: SizeConfig.smallFontSize)),
                  ],
                )),
            Flexible(
              flex: 2,
                child: PrimaryButton(
                    Strings.hotelReserveButton, () => Navigator.pushNamed(context, Routes.hotelBooking))),
          ],
        ),
        const SizedBox(height: 20),
      ],
    );
  }

  final mainImage = SizedBox(
      width: double.infinity,
      child: Image.asset(Images.hotelImage1, fit: BoxFit.cover));

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(),
      extendBodyBehindAppBar: true,
        body: SingleChildScrollView(
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Stack(alignment: Alignment.bottomRight, children: [
          CarouselImages([
            mainImage,
            mainImage,
            mainImage,
            mainImage,
            mainImage,
          ]),
          GestureDetector(
            onTap: () => Navigator.pushNamed(context, Routes.detailsGallery),
            child: Container(
                margin: EdgeInsets.all(SizeConfig.smallMargin),
                padding: EdgeInsets.all(SizeConfig.smallPadding),
                decoration: const BoxDecoration(
                    shape: BoxShape.circle, color: Colors.black),
                child: const Icon(Icons.grid_on, color: Colors.white)),
          ),
        ]),
        Container(
          margin: SizeConfig.safeContainerMargin,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _topSection(),
              Container(
                margin: EdgeInsets.symmetric(vertical: SizeConfig.largeMargin),
                child: Flex(
                  direction: Axis.horizontal,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    _tabs('Facilities', Images.tabFacilities,
                        Routes.detailsFacilities, context),
                    _tabs('Reviews', Images.tabReviews, Routes.detailsReviews,
                        context),
                    _tabs('Map', Images.tabMap, Routes.detailsReviews, context),
                    _tabs('Location', Images.tabLocation,
                        Routes.detailsLocation, context),
                  ],
                ),
              ),
              _room(context),
              _room(context),
              SecondaryButton(Strings.hotelMoreRooms, () {}),
            ],
          ),
        ),
      ]),
    ));
  }
}
