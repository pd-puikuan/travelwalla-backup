import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:travelwalla_mobile/utils/routes.dart';
import 'package:travelwalla_mobile/utils/size_config.dart';

import '../../widgets/chips/campaign_chip.dart';

import '../../utils/strings.dart';

class HotelListingScreen extends StatelessWidget {
  const HotelListingScreen({Key? key}) : super(key: key);

  _generateStars(stars) {
    var emptyStars = 5 - stars;
    List<Widget> totalStars = [];

    for (var i = 0; i < stars; i++) {
      totalStars.add(
        Icon(Icons.star_rounded,
            color: Colors.amber, size: SizeConfig.mediumFontSize),
      );
    }

    for (var i = 0; i < emptyStars; i++) {
      totalStars.add(Icon(Icons.star_rounded,
          color: Colors.grey, size: SizeConfig.mediumFontSize));
    }

    return totalStars;
  }

  _hotelCard(context, hotel) {
    var roomPrice = hotel['room_cheaped_price'].toStringAsFixed(2);
    var roomMainPrice = roomPrice;
    if (roomPrice != null && roomPrice.length >= 4) {
      roomMainPrice = roomPrice.substring(0, roomPrice.length - 3);
    }
    var roomDecimalPrice = roomPrice.substring(roomPrice.length - 3);
    return GestureDetector(
      onTap: () => Navigator.pushNamed(context, Routes.hotelDetails),
      child: Flex(
        direction: Axis.horizontal,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            flex: 7,
            child: FittedBox(
              fit: BoxFit.fitWidth,
              child: Container(
                  margin: EdgeInsets.only(right: SizeConfig.smallMargin),
                  width: SizeConfig.safeBlockHorizontal * 30,
                  height: SizeConfig.safeBlockHorizontal * 25,
                  decoration:
                      BoxDecoration(borderRadius: BorderRadius.circular(10.0)),
                  clipBehavior: Clip.hardEdge,
                  child: Image.network(
                    hotel['img'] ??
                        'https://icon-library.com/images/photo-placeholder-icon/photo-placeholder-icon-6.jpg',
                    fit: BoxFit.cover,
                  )),
            ),
          ),
          Expanded(
            flex: 8,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(hotel['hotel_name'],
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      height: SizeConfig.textHeight,
                      fontSize: SizeConfig.subtitleFontSize,
                    )),
                Text(hotel['location'],
                    style: TextStyle(
                        height: SizeConfig.textHeight,
                        fontSize: SizeConfig.smallFontSize)),
                FittedBox(
                    fit: BoxFit.fitWidth,
                    child: Flex(direction: Axis.horizontal, children: [
                      Row(children: _generateStars(hotel['star'])),
                      // Text(' (1000 reviews)',
                      //     style: TextStyle(fontSize: SizeConfig.smallFontSize)),
                    ])),
                CampaignChipSet(const [
                  '24HOURSALE | Coupon Applied!',
                  '20DISCOUNT',
                  'FREECOUPON',
                  'FREEBREAKFASTHITEA'
                ]),
                FittedBox(
                  fit: BoxFit.fitWidth,
                  child: RichText(
                    text: TextSpan(children: [
                      WidgetSpan(
                          child: Container(
                        margin: EdgeInsets.symmetric(
                            horizontal: SizeConfig.xSmallMargin),
                        child: Text(
                            'RM${hotel['sale_price'].toStringAsFixed(2)}',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: SizeConfig.smallFontSize,
                                decoration: TextDecoration.lineThrough)),
                      )),
                      TextSpan(
                          text: 'RM',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: SizeConfig.mediumFontSize,
                              fontWeight: FontWeight.bold)),
                      TextSpan(
                          text: roomMainPrice,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: SizeConfig.largeFontSize,
                              fontWeight: FontWeight.bold)),
                      TextSpan(
                          text: roomDecimalPrice,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: SizeConfig.mediumFontSize,
                              fontWeight: FontWeight.bold)),
                    ]),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final arguments = (ModalRoute.of(context)?.settings.arguments ??
        <String, dynamic>{}) as Map;
    var data = json.decode(arguments['data']) as Map<String, dynamic>;
    return Scaffold(
      appBar: AppBar(
          title: Text(
        Strings.appbarHotelListing,
        style: TextStyle(fontSize: SizeConfig.mediumFontSize),
      )),
      body: Container(
        margin: SizeConfig.safeContainerMargin,
        child: data['data'].isNotEmpty
            ? ListView.builder(
                itemCount: data.length,
                itemBuilder: (context, index) {
                  return Column(
                    children: [
                      _hotelCard(context, data['data'][index]),
                      const Divider(),
                    ],
                  );
                })
            : const SizedBox.shrink(),
      ),
    );
  }
}
