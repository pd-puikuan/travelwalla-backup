import 'package:flutter/material.dart';

import '../../widgets/main_scaffold.dart';
import '../../widgets/buttons/primary_button.dart';
import '../../widgets/chips/booking_chip.dart';
import '../../widgets/snippets/snippet_with_subheader.dart';
import '../../widgets/snippets/snippet_with_image.dart';
import '../../utils/strings.dart';
import '../../utils/images.dart';

class HotelPaymentScreen extends StatelessWidget {
  const HotelPaymentScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MainScaffold(
      extendBack: true,
      child: Column(
        children: [
          SnippetWithImage(
            imageUri: Images.paymentSuccess,
            title: Strings.paymentSuccessHeader,
            subtitle: '${Strings.currency} 1669.00',
            desc: Strings.paymentSuccessText,
            altView: true,
          ),
          const Text(
            'The Westin Langkawi Resort & Spa',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          const BookingChip('H120243', 500),
          SnippetWithSubheader(
              title: Strings.paymentHotelHeader,
              subtitle: 'The Westin Langkawi Resort & Spa',
              desc: '25 Jun - 29 Jun (5 Nights)'),
          SnippetWithSubheader(
            title: Strings.paymentRoomHeader,
            subtitle: 'Guest Room',
            desc: '2 Adults, sea view',
          ),
          SnippetWithSubheader(
            title: Strings.paymentPaymentHeader,
            subtitle: 'Visa card ending *****1234',
            desc: Strings.paymentSuccessDesc,
          ),
          const SizedBox(height: 20),
          PrimaryButton(Strings.paymentBookingButton, () => Navigator.pop(context)),
        ],
      ),
    );
  }
}
