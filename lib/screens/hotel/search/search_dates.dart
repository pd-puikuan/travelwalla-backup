import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../models/search.dart';

import '../../../widgets/buttons/primary_button.dart';

import '../../../utils/strings.dart';
import '../../../utils/size_config.dart';
import '../../../utils/extensions.dart';

import 'package:syncfusion_flutter_datepicker/datepicker.dart';

class SearchDatesScreen extends StatefulWidget {
  const SearchDatesScreen({Key? key}) : super(key: key);

  @override
  State<SearchDatesScreen> createState() => _SearchDatesScreenState();
}

class _SearchDatesScreenState extends State<SearchDatesScreen> {
  int numberOfNights = 0;
  DateTime? initialStart;
  DateTime? initialEnd;

  @override
  void initState() {
    super.initState();
    initialStart = DateTime(2000, 1, 1, 0, 0, 0, 0, 0);
    initialEnd = DateTime(2000, 1, 1, 0, 0, 0, 0, 0);
  }

  void _onSelectionChanged(DateRangePickerSelectionChangedArgs args) {
    var searchProvider = Provider.of<SearchModel>(context, listen: false);
    setState(() {
      searchProvider.hotelDateStart = args.value.startDate;
      searchProvider.hotelDateEnd = args.value.endDate;
      if (searchProvider.hotelDateEnd != null &&
          searchProvider.hotelDateStart != null) {
        numberOfNights = searchProvider.hotelDateEnd
            .difference(searchProvider.hotelDateStart)
            .inDays;
        searchProvider.updateSelectedDates(
            searchProvider.hotelDateStart, searchProvider.hotelDateEnd);
      } else {
        numberOfNights = 0;
      }
    });
  }

  _nightsText(amount) {
    var nightText = amount > 1 ? 'Nights' : 'Night';
    if (amount > 0) {
      return '($amount $nightText)';
    }
    return '';
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
          title: Text('29 Jun Wed - 30 Jul Sat',
              style: TextStyle(fontSize: SizeConfig.mediumFontSize))),
      body: Container(
        margin: SizeConfig.safeContainerMargin,
        child: Column(
          children: [
            Expanded(
              child: SfDateRangePicker(
                enableMultiView: true,
                enablePastDates: false,
                navigationDirection:
                    DateRangePickerNavigationDirection.vertical,
                viewSpacing: 10,
                selectionMode: DateRangePickerSelectionMode.range,
                headerStyle: const DateRangePickerHeaderStyle(
                    textAlign: TextAlign.center,
                    textStyle: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                    )),
                onSelectionChanged: _onSelectionChanged,
              ),
            ),
            PrimaryButton(
                '${Strings.done.toCapitalized()} ${_nightsText(numberOfNights)}',
                () => Navigator.pop(context)),
          ],
        ),
      ),
    );
  }
}
