import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';

import '../../../models/location_search.dart';
import '../../../models/search.dart';
import '../../../utils/strings.dart';

// ignore: must_be_immutable
class SearchLocationScreen extends StatefulWidget {
  const SearchLocationScreen({Key? key}) : super(key: key);

  @override
  State<SearchLocationScreen> createState() => _SearchLocationScreenState();
}

class _SearchLocationScreenState extends State<SearchLocationScreen> {
  final _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    Provider.of<SearchModel>(context,listen: false).loadHistory();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      Provider.of<SearchModel>(context,listen: false).loadHistory();
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Container(
          height: 50,
          decoration: BoxDecoration(
            border: Border.all(
              width: 1.0,
              style: BorderStyle.solid,
              color: Colors.grey.shade400,
            ),
            borderRadius: BorderRadius.circular(10),
          ),
          child: TextField(
            controller: _controller,
            readOnly: true,
            onTap: () async {
              // generate a new token here
              final sessionToken = const Uuid().v4();
              final result = await showSearch(
                context: context,
                delegate: LocationSearch(sessionToken),
              );
              // This will change the text displayed in the TextField
              if (result != null) {
                setState(() {
                  _controller.text = result.description;
                });
              }
            },
            decoration: InputDecoration(
              hintText: Strings.formEnterDestination,
              border: InputBorder.none,
            ),
          ),
        ),
        leading: GestureDetector(
            onTap: () => Navigator.of(context).pop(), child: const Icon(Icons.close)),
      ),
      body: Container(
        margin: const EdgeInsets.only(left: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 20.0),
            Provider.of<SearchModel>(context,listen: false).generateInitialView(context),
          ],
        ),
      ),
    );
  }
}
