import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../models/search.dart';
import '../../../utils/strings.dart';
import '../../../utils/size_config.dart';
import '../../../utils/extensions.dart';

import '../../../widgets/buttons/primary_button.dart';
import '../../../widgets/forms/numeric_input.dart';

class SearchRoomsScreen extends StatelessWidget {
  const SearchRoomsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final searchModel = Provider.of<SearchModel>(context, listen: true);
    return Scaffold(
      appBar: AppBar(
          title: Text(Strings.appbarSearchRooms,
              style: TextStyle(fontSize: SizeConfig.mediumFontSize))),
      body: Container(
        margin: SizeConfig.safeContainerMargin,
        height: MediaQuery.of(context).size.height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              children: [
                NumericInput(Strings.searchRooms, searchModel.roomCount, 'room'),
                NumericInput(Strings.searchAdults, searchModel.adultCount, 'adult'),
                NumericInput(Strings.searchChildren, searchModel.childrenCount, 'children'),
                NumericInput(Strings.searchInfants, searchModel.infantCount, 'infant'),
              ],
            ),
            PrimaryButton(Strings.done.toCapitalized(), () => searchModel.updateSelectedAmount(context)),
          ],
        ),
      ),
    );
  }
}
