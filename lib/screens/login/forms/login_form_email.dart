import 'package:flutter/material.dart';

import '../../../utils/strings.dart';
import 'package:provider/provider.dart';
import '../../../models/email.dart';

import '../../../widgets/forms/text_input.dart';

class LoginFormEmail extends StatefulWidget {
  const LoginFormEmail({Key? key}) : super(key: key);

  @override
  State<LoginFormEmail> createState() => _LoginFormEmailState();
}

class _LoginFormEmailState extends State<LoginFormEmail> {

  @override
  Widget build(BuildContext context) {
    return TextInput(
      controller:
          Provider.of<EmailModel>(context, listen: false).emailController,
      enablePrefix: true,
      prefix: const Icon(Icons.email),
      label: Strings.formEmail,
    );
  }
}
