import 'package:flutter/material.dart';

import '../../../utils/strings.dart';
import 'package:provider/provider.dart';
import '../../../models/password.dart';

import '../../../widgets/forms/text_input.dart';

class LoginFormPassword extends StatefulWidget {
  const LoginFormPassword({Key? key}) : super(key: key);

  @override
  State<LoginFormPassword> createState() => _LoginFormPasswordState();
}

class _LoginFormPasswordState extends State<LoginFormPassword> {
  @override
  Widget build(BuildContext context) {
    return TextInput(
      controller:
          Provider.of<PasswordModel>(context, listen: false).passwordController,
      enablePrefix: true,
      prefix: const Icon(Icons.lock),
      enableSuffix: true,
      suffix: const Icon(Icons.remove_red_eye_outlined),
      label: Strings.formPassword,
    );
  }
}
