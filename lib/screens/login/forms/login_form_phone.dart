import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../../models/phone.dart';

class LoginFormPhone extends StatefulWidget {
  const LoginFormPhone({Key? key}) : super(key: key);

  @override
  State<LoginFormPhone> createState() => _LoginFormPhoneState();
}

class _LoginFormPhoneState extends State<LoginFormPhone> {

  void updateCountry(country) {
    Provider.of<PhoneModel>(context, listen: false)
        .updateFormValue('phoneCode', '+${country.phoneCode}');
  }

  @override
  Widget build(BuildContext context) {
    return Provider.of<PhoneModel>(context,listen: true).generatePhoneFields(context, updateCountry);
  }
}
