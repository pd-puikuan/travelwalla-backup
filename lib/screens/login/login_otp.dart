import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../utils/strings.dart';
import '../../utils/size_config.dart';
import '../../../models/otp.dart';

import '../../widgets/main_scaffold.dart';
import '../../widgets/buttons/primary_button.dart';

class LoginOtpScreen extends StatefulWidget {
  const LoginOtpScreen({Key? key}) : super(key: key);

  @override
  State<LoginOtpScreen> createState() => _LoginOtpScreenState();
}

class _LoginOtpScreenState extends State<LoginOtpScreen> {
    @override
  void initState() {
    super.initState();
    Provider.of<OtpModel>(context, listen: false).initOtpController();
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    Provider.of<OtpModel>(context, listen: false)
        .disposeOtpController();
    super.dispose();
  }

  _otpInput(controller) {
    return Container(
      margin: EdgeInsets.symmetric(
          horizontal: SizeConfig.smallMargin, vertical: SizeConfig.largeMargin),
      height: 50,
      width: 50,
      child: TextFormField(
        textAlign: TextAlign.center,
        keyboardType: TextInputType.number,
        controller: controller,
        maxLength: 1,
        decoration: InputDecoration(
          border: const OutlineInputBorder(),
          counterText: '',
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: Colors.grey.shade400),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
        final arguments = (ModalRoute.of(context)?.settings.arguments ??
        <String, dynamic>{}) as Map;
    SizeConfig().init(context);
    return MainScaffold(
      title: Strings.appbarSignIn,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          RichText(
              text: TextSpan(children: [
            TextSpan(
                text: '${Strings.verificationText} ',
                style: TextStyle(
                    color: Colors.black, height: SizeConfig.textHeight)),
            TextSpan(
                text: '+60123456789.',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    height: SizeConfig.textHeight)),
          ])),
          FittedBox(
            fit: BoxFit.fitWidth,
            child: Flex(direction: Axis.horizontal, children: [
              _otpInput(Provider.of<OtpModel>(context, listen: false).otp1Controller),
              _otpInput(Provider.of<OtpModel>(context, listen: false).otp2Controller),
              _otpInput(Provider.of<OtpModel>(context, listen: false).otp3Controller),
              _otpInput(Provider.of<OtpModel>(context, listen: false).otp4Controller),
              _otpInput(Provider.of<OtpModel>(context, listen: false).otp5Controller),
              _otpInput(Provider.of<OtpModel>(context, listen: false).otp6Controller),
            ]),
          ),
          const SizedBox(height: 10),
          Text('${Strings.verificationResend} 55s'),
          PrimaryButton("Verify OTP", () => Provider.of<OtpModel>(context, listen: false).verifyOtp(arguments, context)),
        ],
      ),
    );
  }
}
