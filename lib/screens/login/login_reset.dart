import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../utils/strings.dart';
import '../../widgets/main_scaffold.dart';
import '../../widgets/buttons/primary_button.dart';

import '../../../models/email.dart';
import '../../../models/phone.dart';

import 'forms/login_form_phone.dart';
import 'forms/login_form_email.dart';

class LoginResetScreen extends StatefulWidget {
  const LoginResetScreen({Key? key}) : super(key: key);

  @override
  State<LoginResetScreen> createState() => _LoginResetScreenState();
}

class _LoginResetScreenState extends State<LoginResetScreen> {
  var isPhone = true;

  _updateMedium() {
    setState(() {
      isPhone = !isPhone;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MainScaffold(
      title: Strings.appbarForgotPassword,
      child: Column(
        children: [
          Text(
            Strings.resetPasswordText1,
          ),
          isPhone ? const LoginFormPhone() : const LoginFormEmail(),
          PrimaryButton(
              isPhone
                  ? Strings.verificationButton
                  : Strings.resetPasswordButton2,
              isPhone
                  ? () => Provider.of<PhoneModel>(context, listen: false)
                      .forgotPassword(context)
                  : () => Provider.of<EmailModel>(context, listen: false)
                      .forgotPassword(context)),
          const SizedBox(height: 20),
          GestureDetector(
            onTap: () => _updateMedium(),
            child: Text(
                isPhone ? Strings.resetWithEmail : Strings.resetWithPhone,
                style: TextStyle(color: Theme.of(context).colorScheme.primary)),
          ),
        ],
      ),
    );
  }
}
