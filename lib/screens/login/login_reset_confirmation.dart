import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'dart:convert';

import '../../utils/strings.dart';
import '../../widgets/main_scaffold.dart';
import '../../widgets/buttons/primary_button.dart';

import '../../../models/password.dart';

import '../../utils/size_config.dart';

import '../../widgets/forms/text_input.dart';

class LoginResetConfirmationScreen extends StatefulWidget {
  const LoginResetConfirmationScreen({Key? key}) : super(key: key);

  @override
  State<LoginResetConfirmationScreen> createState() =>
      _LoginResetConfirmationScreenState();
}

class _LoginResetConfirmationScreenState
    extends State<LoginResetConfirmationScreen> {
  @override
  void initState() {
    super.initState();
    Provider.of<PasswordModel>(context, listen: false)
        .initResetPasswordController();
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    Provider.of<PasswordModel>(context, listen: false)
        .disposeResetPasswordController();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    final arguments = (ModalRoute.of(context)?.settings.arguments ??
        <String, dynamic>{}) as Map;
    var data = json.decode(arguments['data']) as Map<String, dynamic>;
    
    return MainScaffold(
      title: Strings.appbarForgotPassword,
      child: Column(
        children: [
          TextInput(
            controller: Provider.of<PasswordModel>(context, listen: false)
                .newPasswordController,
            enablePrefix: true,
            prefix: const Icon(Icons.lock),
            enableSuffix: true,
            suffix: const Icon(Icons.remove_red_eye_outlined),
            label: Strings.formNewPassword,
          ),
          TextInput(
            controller: Provider.of<PasswordModel>(context, listen: false)
                .confirmPasswordController,
            enablePrefix: true,
            prefix: const Icon(Icons.lock),
            enableSuffix: true,
            suffix: const Icon(Icons.remove_red_eye_outlined),
            label: Strings.formConfirmNewPassword,
          ),
          Text(Strings.helperPassword,
              style: TextStyle(fontSize: SizeConfig.smallFontSize)),
          PrimaryButton(Strings.resetPasswordButton1,
              () => Provider.of<PasswordModel>(context, listen: false)
                .resetPassword(context, data['resetPasswordToken'])),
        ],
      ),
    );
  }
}
