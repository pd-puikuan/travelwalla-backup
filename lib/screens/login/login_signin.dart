import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'sections/section_signup.dart';
import 'sections/section_email.dart';
import 'sections/section_phone_pw.dart';
import 'sections/section_phone_code.dart';
import 'sections/section_social.dart';

import '../../../models/phone.dart';
import '../../../models/email.dart';
import '../../../models/password.dart';

import '../../utils/strings.dart';
import '../../utils/size_config.dart';

import '../../widgets/snippets/snippet_tnc.dart';
import '../../widgets/snippets/snippet_join.dart';

class SigninScreen extends StatefulWidget {
  const SigninScreen({Key? key}) : super(key: key);

  @override
  State<SigninScreen> createState() => _SigninScreenState();
}

class _SigninScreenState extends State<SigninScreen> {
  @override
  void initState() {
    super.initState();
    Provider.of<PhoneModel>(context, listen: false).initPhoneLoginController();
    Provider.of<EmailModel>(context, listen: false).initEmailLoginController();
    Provider.of<PasswordModel>(context, listen: false).initPasswordController();
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    Provider.of<PhoneModel>(context, listen: false)
        .disposePhoneLoginController();
    Provider.of<EmailModel>(context, listen: false)
        .disposeEmailLoginController();
    Provider.of<PasswordModel>(context, listen: false)
        .disposePasswordController();
    super.dispose();
  }

  _promptText(medium, context) {
    var prevMedium =
        Provider.of<PhoneModel>(context, listen: false).prevLoginMedium;
    print('Prev: $prevMedium, Now: $medium');
    var action = medium == 'signUp' ? prevMedium : 'signUp';
    return Align(
      alignment: Alignment.center,
      child: RichText(
        text: TextSpan(
          children: [
            TextSpan(
                text:
                    '${medium == 'signUp' ? Strings.signupAccExist1 : Strings.loginSignUpPrompt1} ',
                style: const TextStyle(color: Colors.black)),
            TextSpan(
                text:
                    '${medium == 'signUp' ? Strings.signupAccExist2 : Strings.loginSignUpPrompt2} ',
                recognizer: TapGestureRecognizer()
                  ..onTap = () =>
                      Provider.of<PhoneModel>(context, listen: false)
                          .updateLoginMedium(action, medium),
                style: TextStyle(
                    color: Theme.of(context).colorScheme.primary,
                    fontWeight: FontWeight.bold)),
          ],
        ),
      ),
    );
  }

  _generateFormView(medium) {
    switch (medium) {
      case 'phoneCode':
        return SectionPhoneCode(_scaffoldKey);
      case 'phonePw':
        return const SectionPhonePw();
      case 'email':
        return const SectionEmail();
      case 'signUp':
        return const SectionSignup();
      default:
        return const SectionPhonePw();
    }
  }

  final _formKey = GlobalKey<FormState>();

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var loginMedium =
        Provider.of<PhoneModel>(context, listen: true).loginMedium;
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
            title: Text(Strings.appbarSignUp,
                style: TextStyle(fontSize: SizeConfig.mediumFontSize))),
        body: Container(
          margin: SizeConfig.safeContainerMargin,
          height: MediaQuery.of(context).size.height,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SnippetJoin(),
                  Form(key: _formKey, child: _generateFormView(loginMedium)),
                  loginMedium != 'signUp'
                      ? const SectionSocial()
                      : const SizedBox.shrink(),
                  const SizedBox(height: 30),
                  _promptText(loginMedium, context),
                ],
              ),
              const SnippetTnc(),
            ],
          ),
        ));
  }
}
