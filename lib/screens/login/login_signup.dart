import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'forms/login_form_phone.dart';
import 'forms/login_form_email.dart';
import 'forms/login_form_password.dart';
import '../../../models/email.dart';
import '../../../models/phone.dart';

import '../../utils/strings.dart';
import '../../utils/size_config.dart';

import '../../widgets/forms/text_input.dart';

import '../../widgets/main_scaffold.dart';
import '../../widgets/buttons/primary_button.dart';
import '../../widgets/snippets/snippet_tnc.dart';
import '../../widgets/snippets/snippet_join.dart';

class SignupScreen extends StatelessWidget {
  const SignupScreen({Key? key}) : super(key: key);

  _email() {
    return Column(
      children: [
        TextInput(
          controller: TextEditingController(),
          enablePrefix: true,
          prefix: const Icon(Icons.person_outline),
          label: Strings.formFirstName,
        ),
        TextInput(
          controller: TextEditingController(),
          enablePrefix: true,
          prefix: const Icon(Icons.person_outline),
          label: Strings.formLastName,
        ),
        const LoginFormEmail(),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    final arguments = (ModalRoute.of(context)?.settings.arguments ??
        <String, dynamic>{}) as Map;
    return MainScaffold(
      title: Strings.appbarSignUp,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SnippetJoin(),
              arguments['medium'] == 'email'
                  ? _email()
                  : const LoginFormPhone(),
              const LoginFormPassword(),
              TextInput(
                controller: TextEditingController(),
                enablePrefix: true,
                prefix: const Icon(Icons.lock),
                enableSuffix: true,
                suffix: const Icon(Icons.remove_red_eye_outlined),
                label: Strings.formConfirmPassword,
              ),
              Text(Strings.helperPassword,
                  style: TextStyle(fontSize: SizeConfig.smallFontSize)),
              Container(
                margin: EdgeInsets.symmetric(vertical: SizeConfig.mediumMargin),
                child: Flex(
                    direction: Axis.horizontal,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Checkbox(
                        value: true,
                        shape: const CircleBorder(),
                        onChanged: (bool? value) {},
                      ),
                      Flexible(
                          child: Text(Strings.helperSpecialOffers,
                              style: TextStyle(
                                  fontSize: SizeConfig.smallFontSize))),
                    ]),
              ),
              PrimaryButton(
                  Strings.signupButton,
                  arguments['medium'] == 'email' ? () => Provider.of<EmailModel>(context, listen: false)
                      .checkAccount(context) : () => Provider.of<PhoneModel>(context, listen: false)
                      .checkAccount(context)),
            ],
          ),
          const SnippetTnc(),
        ],
      ),
    );
  }
}
