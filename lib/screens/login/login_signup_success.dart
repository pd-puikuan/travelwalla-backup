import 'package:flutter/material.dart';

import '../../utils/images.dart';
import '../../utils/strings.dart';
import '../../utils/routes.dart';
import '../../utils/size_config.dart';

import '../../widgets/snippets/snippet_with_image.dart';
import '../../widgets/buttons/primary_button.dart';

class SignupSuccessScreen extends StatelessWidget {
  const SignupSuccessScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      extendBodyBehindAppBar: true,
      body: Container(
        margin: SizeConfig.safeContainerMargin,
        height: MediaQuery.of(context).size.height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SnippetWithImage(
              imageUri: Images.paymentSuccess,
              title: Strings.signupSuccessHeader,
              desc: Strings.signupSuccessText,
            ),
            PrimaryButton(Strings.backToHome, () => Navigator.pushNamed(context, Routes.home)),
          ],
        ),
      ),
    );
  }
}
