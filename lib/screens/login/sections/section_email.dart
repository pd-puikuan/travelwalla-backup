import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../forms/login_form_email.dart';
import '../forms/login_form_password.dart';
import '../../../models/email.dart';

import '../../../utils/strings.dart';
import '../../../utils/size_config.dart';
import '../../../utils/routes.dart';

import '../../../widgets/buttons/primary_button.dart';

class SectionEmail extends StatelessWidget {
  const SectionEmail({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        const LoginFormEmail(),
        const LoginFormPassword(),
        GestureDetector(
          onTap: () => Navigator.pushNamed(context, Routes.loginReset),
          child: Text(Strings.loginForgotPassword,
              style: TextStyle(
                  height: SizeConfig.textHeight,
                  color: Theme.of(context).colorScheme.primary)),
        ),
        const SizedBox(height: 15),
        PrimaryButton(Strings.loginText, () => Provider.of<EmailModel>(context, listen: false).loginAccount(context)),
      ],
    );
  }
}
