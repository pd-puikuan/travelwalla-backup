import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../models/phone.dart';

import '../../../utils/strings.dart';
import '../../../utils/size_config.dart';

import '../forms/login_form_phone.dart';
import '../../../widgets/buttons/primary_button.dart';

class SectionPhoneCode extends StatelessWidget {
  final formKey;
  const SectionPhoneCode(this.formKey, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(formKey.currentContext);
    var loginMedium =
        Provider.of<PhoneModel>(formKey.currentContext, listen: true).loginMedium;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        const LoginFormPhone(),
        PrimaryButton(Strings.verificationButton, () => Provider.of<PhoneModel>(formKey.currentContext, listen: false).checkAccount(context)),
        const SizedBox(height: 15),
        Align(
          alignment: Alignment.center,
          child: GestureDetector(
            onTap: () => Provider.of<PhoneModel>(formKey.currentContext, listen: false)
                .updateLoginMedium('phonePw', loginMedium),
            child: Text(Strings.loginWithPassword,
                style: TextStyle(
                    height: SizeConfig.textHeight,
                    color: Theme.of(formKey.currentContext).colorScheme.primary)),
          ),
        ),
      ],
    );
  }
}
