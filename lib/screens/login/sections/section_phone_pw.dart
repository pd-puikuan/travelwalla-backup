import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../models/phone.dart';

import '../forms/login_form_phone.dart';
import '../forms/login_form_password.dart';

import '../../../utils/strings.dart';
import '../../../utils/size_config.dart';
import '../../../utils/routes.dart';

import '../../../widgets/buttons/primary_button.dart';

class SectionPhonePw extends StatelessWidget {
  const SectionPhonePw({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    // var loginMedium =
        // Provider.of<PhoneModel>(context, listen: true).loginMedium;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        const LoginFormPhone(),
        const LoginFormPassword(),
        GestureDetector(
          onTap: () => Navigator.pushNamed(context, Routes.loginReset),
          child: Text(Strings.loginForgotPassword,
              style: TextStyle(
                  height: SizeConfig.textHeight,
                  color: Theme.of(context).colorScheme.primary)),
        ),
        const SizedBox(height: 15),
        PrimaryButton(Strings.loginText, () => Provider.of<PhoneModel>(context, listen: false).loginAccount(context)),
        // const SizedBox(height: 15),
        // Align(
        //   alignment: Alignment.center,
        //   child: GestureDetector(
        //     onTap: () => Provider.of<PhoneModel>(context, listen: false)
        //         .updateLoginMedium('phoneCode', loginMedium),
        //     child: Text(Strings.loginWithVerification,
        //         style: TextStyle(
        //             height: SizeConfig.textHeight,
        //             color: Theme.of(context).colorScheme.primary)),
        //   ),
        // ),
      ],
    );
  }
}
