import 'package:flutter/material.dart';

import '../../../utils/strings.dart';
import '../../../utils/size_config.dart';
import '../../../utils/routes.dart';

import '../../../widgets/buttons/social_button.dart';

class SectionSignup extends StatelessWidget {
  const SectionSignup({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(Strings.signupText,
            style: TextStyle(height: SizeConfig.textHeight)),
        const SizedBox(height: 30),
        Flex(
          direction: Axis.horizontal,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SocialButton(
                const Icon(Icons.email),
                () => Navigator.pushNamed(context, Routes.loginSignup,
                    arguments: {'medium': 'email'})),
            SocialButton(
                const Icon(Icons.phone_android),
                () => Navigator.pushNamed(context, Routes.loginSignup,
                    arguments: {'medium': 'phone'})),
            SocialButton(const Icon(Icons.facebook), () {}),
            SocialButton(const Icon(Icons.g_mobiledata), () {}),
            SocialButton(const Icon(Icons.apple), () {}),
          ],
        ),
      ],
    );
  }
}
