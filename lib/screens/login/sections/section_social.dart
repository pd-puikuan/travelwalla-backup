import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:provider/provider.dart';

import '../../../models/phone.dart';
import '../../../models/social.dart';

import '../../../utils/strings.dart';
import '../../../utils/size_config.dart';
import '../../../widgets/buttons/social_button.dart';
import '../../../models/alert.dart';

class SectionSocial extends StatefulWidget {
  const SectionSocial({Key? key}) : super(key: key);

  @override
  State<SectionSocial> createState() => _SectionSocialState();
}

class _SectionSocialState extends State<SectionSocial> {
  _generateMedium(medium, context) {
    if (medium == 'phoneCode' || medium == 'phonePw') {
      return SocialButton(
          const Icon(Icons.email),
          () => Provider.of<PhoneModel>(context, listen: false)
              .updateLoginMedium('email', medium));
    } else if (medium == 'email') {
      return SocialButton(
          const Icon(Icons.phone_android),
          () => Provider.of<PhoneModel>(context, listen: false)
              .updateLoginMedium('phonePw', medium));
    } else {
      return const SizedBox.shrink();
    }
  }

  final GoogleSignIn _googleSignIn = GoogleSignIn(
    scopes: [
      'email',
      'https://www.googleapis.com/auth/contacts.readonly',
    ],
  );

  Future<void> googleLogin() async {
    try {
      await _googleSignIn.signIn();
      print('Sign in successful');
    } catch (e) {
      Alert().showMyDialog(context, e);
    }
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var loginMedium =
        Provider.of<PhoneModel>(context, listen: true).loginMedium;
    return Column(
      children: [
        const SizedBox(height: 30),
        Flex(direction: Axis.horizontal, children: [
          const Expanded(child: Divider()),
          Container(
              margin: EdgeInsets.symmetric(horizontal: SizeConfig.mediumMargin),
              child: Text(Strings.or)),
          const Expanded(child: Divider()),
        ]),
        const SizedBox(height: 30),
        Flex(
          direction: Axis.horizontal,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            _generateMedium(loginMedium, context),
            SocialButton(
                const Icon(Icons.facebook),
                () async =>
                    await Provider.of<SocialModel>(context, listen: false)
                        .facebookLogin()),
            SocialButton(const Icon(Icons.g_mobiledata), () => googleLogin()),
            SocialButton(const Icon(Icons.apple), () {}),
          ],
        )
      ],
    );
  }
}
