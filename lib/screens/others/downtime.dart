import 'package:flutter/material.dart';

import '../../utils/images.dart';
import '../../utils/strings.dart';

import '../../widgets/main_scaffold.dart';
import '../../widgets/snippets/snippet_with_image.dart';

class DowntimeScreen extends StatelessWidget {
  const DowntimeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MainScaffold(
      extendBack: true,
      child: SnippetWithImage(
        imageUri: Images.paymentDenied,
        title: Strings.technicalIssueHeader,
        desc: Strings.technicalIssueText,
      ),
    );
  }
}
