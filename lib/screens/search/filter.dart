import 'package:flutter/material.dart';

import '../../utils/strings.dart';
import '../../utils/size_config.dart';
import '../../utils/extensions.dart';

import '../../widgets/main_scaffold.dart';
import '../../widgets/chips/filter_chip.dart';
import '../../widgets/buttons/primary_button.dart';

class FilterScreen extends StatefulWidget {
  const FilterScreen({Key? key}) : super(key: key);

  @override
  State<FilterScreen> createState() => _FilterScreenState();
}

class _FilterScreenState extends State<FilterScreen> {
  RangeValues _currentRangeValues = const RangeValues(0, 5000);
  _slider() {
    return RangeSlider(
      values: _currentRangeValues,
      max: 5000,
      divisions: 5000,
      labels: RangeLabels(
        '${Strings.currency}${_currentRangeValues.start.round().toString()}',
        '${Strings.currency}${_currentRangeValues.end.round().toString()}',
      ),
      onChanged: (RangeValues values) {
        setState(() {
          _currentRangeValues = values;
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return MainScaffold(
      title: Strings.appbarFilter,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Price Range',
              style: TextStyle(
                  fontSize: SizeConfig.subtitleFontSize,
                  height: SizeConfig.textHeight)),
          _slider(),
          const Divider(),
          Text('Star Rating',
              style: TextStyle(
                  fontSize: SizeConfig.subtitleFontSize,
                  height: SizeConfig.textHeight)),
          const SizedBox(height: 10),
          Container(
            margin: EdgeInsets.symmetric(horizontal: SizeConfig.mediumMargin),
            child: Flex(
              direction: Axis.horizontal,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Expanded(
                  child: Icon(Icons.star_rounded,
                      size: SizeConfig.largeFontSize * 1.2),
                ),
                Expanded(
                  child: Icon(Icons.star_rounded,
                      size: SizeConfig.largeFontSize * 1.2),
                ),
                Expanded(
                  child: Icon(Icons.star_rounded,
                      size: SizeConfig.largeFontSize * 1.2),
                ),
                Expanded(
                  child: Icon(Icons.star_rounded,
                      size: SizeConfig.largeFontSize * 1.2),
                ),
                Expanded(
                  child: Icon(Icons.star_rounded,
                      size: SizeConfig.largeFontSize * 1.2),
                ),
              ],
            ),
          ),
          const SizedBox(height: 10),
          const Divider(),
          Text('Facilities',
              style: TextStyle(
                  fontSize: SizeConfig.subtitleFontSize,
                  height: SizeConfig.textHeight)),
          const SizedBox(height: 10),
          FilterChipSet(const [
            'Family/child friendly',
            'Swimming pool',
            'Front desk [24-hour]',
            'Airport transfer',
            'Smoking area',
            'Internet',
            'Car park',
            'Gym/fitness'
          ]),
          const SizedBox(height: 10),
          const Divider(),
          Text('Accomodation Type',
              style: TextStyle(
                  fontSize: SizeConfig.subtitleFontSize,
                  height: SizeConfig.textHeight)),
          const SizedBox(height: 10),
          FilterChipSet(const [
            'Hotel',
            'Entire apartment',
            'Resort',
            'Serviced apartment',
            'Guesthouse',
            'Entire villa',
            'Entire house'
          ]),
          const SizedBox(height: 10),
          const Divider(),
          Text('Payment Options',
              style: TextStyle(
                  fontSize: SizeConfig.subtitleFontSize,
                  height: SizeConfig.textHeight)),
          const SizedBox(height: 10),
          FilterChipSet(const [
            'Pay now',
            'Pay at hotel',
            'Book now, pay later',
            'Book without credit card'
          ]),
          const SizedBox(height: 20),
          PrimaryButton(Strings.done.toCapitalized(), () {}),
        ],
      ),
    );
  }
}
