import 'package:flutter/material.dart';

import '../../utils/strings.dart';
import '../../utils/extensions.dart';

import '../../widgets/main_scaffold.dart';
import '../../widgets/chips/filter_chip.dart';
import '../../widgets/buttons/primary_button.dart';

class SortScreen extends StatelessWidget {
  const SortScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MainScaffold(
      title: Strings.appbarSort,
      child: Column(
        children: [
          FilterChipSet(const [
            'Most recommended',
            'Lowest price',
            'Highest popularity',
            'Highest star rating'
          ]),
          const SizedBox(height: 20),
          PrimaryButton(Strings.done.toCapitalized(), () {}),
        ],
      ),
    );
  }
}
