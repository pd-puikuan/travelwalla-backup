class API {
  // API STRINGS
  static String checkExistingAccount = '/api/auth/check-account';
  static String verifyOtp = '/api/auth/verify-otp';
  static String registerUser = '/api/auth/local/register';
  static String forgotPassword = '/api/auth/forgot-password';
  static String resetPassword = '/api/auth/reset-password';

  static String login = '/api/auth/local';

  static String hotelList = '/api/hotels/list?';
  static String hotelFilter = '/api/hotels/filter?';
}