class Constants {

  static String apiBaseUrl = 'https://admin-stg.traveldurian.com';
  static String apiPort = '3004';
  static String apiKeyHeader = '';
  static String contentTypeHeader = '';
  static String authorizationHeader = '';
  static int minQueryLength = 3;
  static double buttonHeight = 55;

}