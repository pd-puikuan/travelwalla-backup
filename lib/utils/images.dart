class Images {
  static String paymentSuccess = 'assets/images/payment-success.png';
  static String paymentPending = 'assets/images/payment-pending.png';
  static String paymentDenied = 'assets/images/payment-denied.png';

  static String hotelImage1 = 'assets/images/hotel-images/1.jpeg';
  static String hotelImage2 = 'assets/images/hotel-images/2.jpeg';
  static String hotelImage3 = 'assets/images/hotel-images/3.jpeg';
  static String hotelImage4 = 'assets/images/hotel-images/4.jpeg';
  static String hotelImage5 = 'assets/images/hotel-images/5.jpeg';

  static String tabFacilities = 'assets/images/tabs/tab-facilities.png';
  static String tabReviews = 'assets/images/tabs/tab-reviews.png';
  static String tabMap = 'assets/images/tabs/tab-map.png';
  static String tabLocation = 'assets/images/tabs/tab-location.png';
}