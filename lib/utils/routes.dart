class Routes {
  static String home = '/';

  static String loginSignup = '/login/signup';
  static String loginSignupSuccess = '/login/signup/sucess';
  static String loginSignin = '/login/signin';
  static String loginReset = '/login/reset';
  static String loginResetConfirmation = '/login/reset/confirmation';
  static String loginOtp = '/login/otp';

  static String hotelSearch = '/hotel/search';
  static String searchRooms = '/search/rooms';
  static String searchDates = '/search/dates';
  static String searchLocation = '/search/location';
  static String hotelListing = '/hotel/listing';
  static String hotelDetails = '/hotel/details';
  static String hotelBooking = '/hotel/booking';
  static String hotelConfirmation = '/hotel/confirmation';
  static String hotelPayment = '/hotel/payment';

  static String detailsRoom = 'hotel/details/room';
  static String detailsGallery = 'hotel/details/gallery';
  static String detailsFacilities = 'hotel/details/facilities';
  static String detailsReviews = 'hotel/details/reviews';
  static String detailsLocation = 'hotel/details/location';

  static String flightSearch = '/flight/search';

  static String sort = '/search/sort';
  static String filter = '/search/filter';
  static String downtime = '/downtime';
}
