import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class SizeConfig {
  late MediaQueryData _mediaQueryData;
  late double screenWidth;
  late double screenHeight;
  late double blockSizeHorizontal;
  late double blockSizeVertical;
  late double _safeAreaHorizontal;
  late double _safeAreaVertical;
  static late double safeBlockHorizontal;
  static late double safeBlockVertical;
  static late EdgeInsets safeContainerMargin;
    static late double xSmallMargin;
  static late double xSmallPadding;
  static late double smallMargin;
  static late double smallPadding;
  static late double mediumMargin;
  static late double mediumPadding;
  static late double largeMargin;
  static late double largePadding;
  static late double largeFontSize;
  static late double semiLargeFontSize;
  static late double mediumFontSize;
  static late double smallFontSize;
  static late double xSmallFontSize;
  static late double headerFontSize;
  static late double subtitleFontSize;
  static late double buttonFontSize;
  static late double textHeight;

  void init(BuildContext context) {
    _mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;
    blockSizeHorizontal = screenWidth / 100;
    blockSizeVertical = screenHeight / 100;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;
    safeBlockHorizontal = (screenWidth - _safeAreaHorizontal) / 100;
    safeBlockVertical = (screenHeight - _safeAreaVertical) / 100;

    safeContainerMargin = EdgeInsets.symmetric(
        vertical: safeBlockVertical * 2, horizontal: safeBlockHorizontal * 6);

    xSmallMargin = screenWidth * 0.01;
    xSmallPadding = screenWidth * 0.01;
    smallMargin = screenWidth * 0.02;
    smallPadding = screenWidth * 0.02;
    mediumMargin = screenWidth * 0.04;
    mediumPadding = screenWidth * 0.04;
    largeMargin = screenWidth * 0.06;
    largePadding = screenWidth * 0.06;

    largeFontSize = screenWidth * 0.08;
    semiLargeFontSize = screenWidth * 0.07;
    mediumFontSize = screenWidth * 0.05;
    smallFontSize = screenWidth * 0.03;
    xSmallFontSize = screenWidth * 0.023;
    headerFontSize = screenWidth * 0.06;
    subtitleFontSize = screenWidth * 0.04;
    buttonFontSize = screenWidth * 0.045;
    textHeight = screenHeight * 0.002;
  }
}
