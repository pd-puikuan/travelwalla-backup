class Strings {
  static String title = 'TravelWalla';
  // Generic Texts
  static String and = 'and';
  static String backToHome = 'Back to Home';
  static String done = 'done';
  static String or = 'or';
  static String getStarted = 'get started';
  static String skip = 'skip';
  static String reviewsSingular = 'review';
  static String reviewsPlural = 'reviews';
  static String currency = 'RM';

  // Appbar Headers
  static String appbarSignUp = 'Sign Up';
  static String appbarSignIn = 'Sign In';
  static String appbarForgotPassword = 'Forgot Password';
  static String appbarProfile = 'My Profile';
  static String appbarSearchHotel = 'Book A Hotel';
  static String appbarSearchRooms = 'Travellers & Rooms';
  static String appbarHotelListing = 'Book A Hotel';
  static String appbarSort = 'Sort';
  static String appbarFilter = 'Filter';
  static String appbarHotelFacilities = 'Facilities';
  static String appbarHotelReviews = 'Reviews';
  static String appbarHotelMap = 'Map';
  static String appbarHotelLocation = 'Location';
  static String appbarHotelRooms = 'Room Detail';
  static String appbarHotelGallery = 'Gallery';
  static String appbarBookingDetails = 'Booking Details';
  static String appbarBookingReview = 'Review Booking Details';
  static String appbarSearchFlight = 'Book A Flight';
  static String appbarSearchPassengers = 'Passengers';
  static String appbarFlightDeparture = 'Book A Flight Departure';
  static String appbarFlightReturn = 'Book A Flight Return';
  static String appbarFlightDetails = 'Flight Details';
  static String appbarPassengerDetails = 'Passenger Details';

  // Onboarding Screen
  static String onboardingHeader1 = 'Booking Hotel';
  static String onboardingText1 =
      'Book your perfect holiday and enjoy great discounts on hotels.';
  static String onboardingHeader2 = 'Begin Your Journey';
  static String onboardingText2 =
      'Use our journey planner to find the best routes.';
  static String onboardingHeader3 = 'Plan Your Adventure';
  static String onboardingText3 =
      'Use our journey planner to find the best routes.';

  // Home Screen
  static String homeHeader = 'Welcome,';
  static String homeText = 'what would you like to plan?';
  static String homeProfileButton = 'My Profile';
  static String homeHotelButton = 'Book A Hotel';
  static String homeFlightButton = 'Book A Flight';
  static String homeJourneyButton = 'Begin Your Journey';
  static String homeCategoryHeader = 'Category';
  static String homeOfferHeader = 'Special Offer';

  // Login Screen - Main
  static String loginHeader1 = 'Join us,';
  static String loginHeader2 = 'manage your journey.';
  static String loginText = 'Login';
  static String loginWithPassword = 'Login with password';
  static String loginWithVerification = 'Login with verification code';
  static String loginSignUpPrompt1 = 'No account yet?';
  static String loginSignUpPrompt2 = 'Sign up here';
  static String loginTncText = 'By signing up or logging in, I accept the';
  static String loginTermsOfService = 'Terms of Service';
  static String loginPrivacyPolicy = 'Privacy Policy';
  static String loginForgotPassword = 'Forgot your password?';

  // Login Screen - Forgot Password
  static String resetPasswordText1 =
      'We\'ll send you a verification code to reset your password.';
  static String resetPasswordText2 =
      'We\'ll send you a verification link to reset your password.';
  static String resetWithEmail = 'Reset via email';
  static String resetWithPhone = 'Reset via phone number';
  static String resetPasswordButton1 = 'Reset Password';
  static String resetPasswordButton2 = 'Send Verification Link';

  // Login Screen - Verification
  static String verificationText = 'A verification code has been sent to';
  static String verificationButton = 'Send Verification Code';
  static String verificationResend = 'Resend code in';

  // Signup Screen
  static String signupText = 'Sign up with your preferred account.';
  static String signupButton = 'Sign Up';
  static String signupAccExist1 = 'Already have an account?';
  static String signupAccExist2 = 'Sign in here';
  static String signupSuccessHeader = 'Congratulations!';
  static String signupSuccessText =
      'Your account has been successfully created.';

  // Search Screen
  static String searchHeader = 'Choose your preferences';
  static String searchButton = 'Search';
  static String searchRecommendation = 'Recommendation';
  static String searchNearMe = 'Near me';
  static String searchRecent = 'Recent Searches';
  static String searchNightSingular = 'Night';
  static String searchNightPlural = 'Nights';
  static String autocompleteTypes = 'landmark|neighborhood|tourist_attraction|lodging|locality';

  // Search Screen - Travellers & Rooms
  static String searchRooms = 'Room(s)';
  static String searchAdults = 'Adult(s) (Ages > 12)';
  static String searchChildren = 'Children (Ages 3-12)';
  static String searchInfants = 'Infants (Ages < 2)';

  // Hotel Listing Screen (Book A Hotel)
  static String hotelMoreCampaigns = 'More Campaigns';

  // Hotel Details Screen
  static String hotelFacilities = 'Facilities';
  static String hotelReviews = 'Reviews';
  static String hotelMap = 'Map';
  static String hotelLocation = 'Location';
  static String hotelRoom = 'Room Details';
  static String hotelMoreRooms = 'More Rooms';
  static String hotelReserveButton = 'Reserve';
  static String hotelIncludedTax = 'tax included';

  // Booking Details Screen
  static String bookingRoomHeader = 'Room Details';
  static String bookingContactHeader = 'Contact Details';
  static String bookingForAnotherPerson = 'I\'m booking for another person';
  static String bookingAddonHeader = 'Add-on Services';
  static String bookingAddonBed = 'Bed type';
  static String bookingAddonSmoking = 'Smoking preferences';
  static String bookingAddonOthers = 'Other add-ons';
  static String bookingCancellationHeader = 'Cancellation Policy';
  static String bookingCancellationOption1 = 'Non-Refundable';
  static String bookingCancellationOption2 = 'Fully refundable before';
  static String bookingInfoHeader = 'Booking Information';
  static String bookingPaymentHeader = 'Payment Options';
  static List<String> bookingPaymentOptions = [
    'Pay Now',
    'Pay at Hotel',
    'Book Now, Pay Later',
    'Book Without Credit Card'
  ];
  static String bookingSummaryHeader = 'Payment Summary';
  static String bookingUseRewardPoints = 'reward points';
  static String bookingContinueButton = 'Continue';

  // Booking Details Screen - Review
  static String bookingReviewPrice = 'Price Details';
  static String bookingDiscountPrice = 'Discount';
  static String bookingTaxes = 'Taxes and fees';
  static String bookingAddOnPrice = 'Add-on services';
  static String bookingTotalPrice = 'Total Price';
  static String bookingTotalPriceText = 'with taxes & fees';
  static String bookingTnc =
      'By clicking the button below, you agree to TravelWalla\'s Terms & Conditions and Privacy Policy.';
  static String bookingPaymentButton = 'Proceed to Payment';

  // After Payment Screen
  static String paymentSuccessHeader = 'Congratulations!';
  static String paymentPendingHeader = 'Transaction in Process...';
  static String paymentDeniedHeader = 'Oops, your payment is denied';
  static String paymentSuccessText =
      'You have successfully booked your stay with';
  static String paymentPendingText =
      'Please review on updated status in My Booking after few minutes.';
  static String paymentDeniedText = 'Please try it again.';
  static String paymentBookingId = 'Booking ID';
  static String paymentHotelHeader = 'Hotel';
  static String paymentRoomHeader = 'Room Details';
  static String paymentPaymentHeader = 'Payment Method';
  static String paymentSuccessDesc =
      'Your booking is now completed. Please show the booking order to the receptionist and finish the payment.';
  static String paymentBookingButton = 'Back to My Booking';
  static String paymentBackButton = 'Back to Payment';

  // Flight
  static String flightSearchHeader = 'Journey';
  static String flightSeatType = 'Seat Type';
  static String flightFlightDeals = 'Flight Deals';
  static String formOneWay = 'One Way';
  static String formRoundTrip = 'Round Trip';
  static String formFlightFrom = 'Flying from';
  static String formFlightTo = 'Flying to';
  static String formFlightDate = 'Date';
  static String formFlightPassengers = 'Passengers';
  static String formSeatEconomy = 'Economy';
  static String formSeatBusiness = 'Business';
  static String formSeatFirstClass = 'First Class';

  // Form Labels
  static String formPhoneNumber = 'Phone number';
  static String formEmail = 'Email address';
  static String formCountryRegionCode = 'Country region code';
  static String formRegionCode = 'Region code';
  static String formCountry = 'Country';
  static String formPassword = 'Password';
  static String formNewPassword = 'New Password';
  static String formConfirmPassword = 'Confirm Password';
  static String formConfirmNewPassword = 'Confirm New Password';
  static String formFullName = 'Full name';
  static String formFirstName = 'First name';
  static String formLastName = 'Last name';
  static String formGoingTo = 'Going to';
  static String formDates = 'Dates';
  static String formTravellers = 'Travellers';
  static String formEnterDestination = 'Enter destination';
  static String formFilter = 'Filter';
  static String formSort = 'Sort';
  static String formRemarks = 'Remarks';
  static String formCouponCode = 'Coupon Code';

  // Technical Issues
  static String technicalIssueHeader = 'We\'ll be right back';
  static String technicalIssueText =
      'We\'re currently experiencing technical issues.\nPlease try again later.\nWe apologize for any incovenience.';
  static String errorHeader = 'Something went wrong.';
  static String errorButton = 'Close';

  // Form Errors
  static String errorInvalidPassword = 'Invalid password.';

  // Form Helpers
  static String helperPassword =
      'Password must be 8-15 characters and include at least 1 number and 1 symbol.';
  static String helperSpecialOffers =
      'I would like to receive special offers, promotion and other information from TravelWalla.';
}
