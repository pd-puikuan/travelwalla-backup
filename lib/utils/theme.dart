import 'package:flutter/material.dart';
import 'constants.dart';

Map<int, Color> primary = {
  50: const Color.fromRGBO(160, 31, 97, .1),
  100: const Color.fromRGBO(160, 31, 97, .2),
  200: const Color.fromRGBO(160, 31, 97, .3),
  300: const Color.fromRGBO(160, 31, 97, .4),
  400: const Color.fromRGBO(160, 31, 97, .5),
  500: const Color.fromRGBO(160, 31, 97, .6),
  600: const Color.fromRGBO(160, 31, 97, .7),
  700: const Color.fromRGBO(160, 31, 97, .8),
  800: const Color.fromRGBO(160, 31, 97, .9),
  900: const Color.fromRGBO(160, 31, 97, 1),
};

double buttonHeight = Constants.buttonHeight;

MaterialColor colorPrimary = MaterialColor(0xFFA01F61, primary);
Color colorSecondary = const Color.fromRGBO(219, 104, 0, 1);
Color colorTertiary = const Color.fromRGBO(45, 156, 219, 1);
Color colorWhite = Colors.white;
Color colorBlack = Colors.black;
Color colorGrey = Colors.grey;
Color colorTransparent = Colors.transparent;

class AppTheme {
  ThemeData themedata = ThemeData(
    primarySwatch: colorPrimary,
    colorScheme: ColorScheme.fromSwatch(primarySwatch: colorPrimary)
        .copyWith(secondary: colorSecondary, tertiary: colorTertiary),
    fontFamily: 'Open Sans',
    appBarTheme: AppBarTheme(
      centerTitle: true,
      color: colorTransparent,
      shadowColor: colorTransparent,
      titleTextStyle: TextStyle(
        fontFamily: 'Open Sans',
        color: colorBlack,
        fontWeight: FontWeight.bold,
      ),
      iconTheme: IconThemeData(color: colorBlack),
    ),
    dividerTheme: DividerThemeData(color: colorGrey),
    textButtonTheme: TextButtonThemeData(
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all(
          colorWhite,
        ),
        minimumSize: MaterialStateProperty.all(
          const Size(double.infinity, 55),
        ),
        shape: MaterialStateProperty.all(
          const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(30.0),
            ),
          ),
        ),
        textStyle: MaterialStateProperty.all(
          const TextStyle(fontWeight: FontWeight.w700),
        ),
      ),
    ),
    scaffoldBackgroundColor: colorWhite,
    // MAIN BUTTON THEME
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all(
          colorPrimary,
        ),
        minimumSize: MaterialStateProperty.all(
          const Size(double.infinity, 55),
        ),
        textStyle: MaterialStateProperty.all(
          const TextStyle(fontWeight: FontWeight.w700),
        ),
      ),
    ),
    outlinedButtonTheme: OutlinedButtonThemeData(
      style: ButtonStyle(
        side: MaterialStateProperty.all(
          BorderSide(
            color: colorGrey,
            style: BorderStyle.solid,
          ),
        ),
        shape: MaterialStateProperty.all(
          const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10.0),
            ),
          ),
        ),
        minimumSize: MaterialStateProperty.all(
          Size(double.infinity, buttonHeight),
        ),
        textStyle: MaterialStateProperty.all(
          const TextStyle(fontWeight: FontWeight.w700),
        ),
      ),
    ),
  );
}
