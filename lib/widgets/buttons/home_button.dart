import 'package:flutter/material.dart';

import '../../utils/size_config.dart';

class HomeButton extends StatelessWidget {
  final String _title;
  final String _route;
  final String _position;
  const HomeButton(this._title, this._route, this._position, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Expanded(
      child: GestureDetector(
        onTap: () => Navigator.of(context).pushNamed(_route),
        child: Container(
          margin: EdgeInsets.only(
              top: SizeConfig.smallMargin,
              bottom: SizeConfig.smallMargin,
              left: _position != 'left' ? SizeConfig.smallMargin : 0,
              right: _position == 'left' ? SizeConfig.smallMargin : 0),
          padding: EdgeInsets.all(SizeConfig.mediumPadding),
          decoration: BoxDecoration(
            color: Theme.of(context).primaryColor.withOpacity(0.2),
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: Text(
            _title,
            style: TextStyle(
                fontSize: SizeConfig.mediumFontSize,
                fontWeight: FontWeight.bold,
                color: Theme.of(context).primaryColor),
          ),
        ),
      ),
    );
  }
}
