import 'package:flutter/material.dart';
import 'package:travelwalla_mobile/utils/size_config.dart';

class PrimaryButton extends StatelessWidget {
  final String _label;
  final VoidCallback _action;

  const PrimaryButton(this._label, this._action, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      margin: EdgeInsets.symmetric(vertical: SizeConfig.smallMargin),
      child: ElevatedButton(
        onPressed: _action,
        child:
            Text(_label, style: TextStyle(fontSize: SizeConfig.buttonFontSize)),
      ),
    );
  }
}
