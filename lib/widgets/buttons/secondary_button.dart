import 'package:flutter/material.dart';
import 'package:travelwalla_mobile/utils/size_config.dart';

class SecondaryButton extends StatelessWidget {
  final String _label;
  final Function _action;

  const SecondaryButton(this._label, this._action, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return OutlinedButton(
      onPressed: () => _action,
      child: Text(_label, style: TextStyle(fontSize: SizeConfig.buttonFontSize)),
    );
  }
}
