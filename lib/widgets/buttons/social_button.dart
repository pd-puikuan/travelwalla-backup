import 'package:flutter/material.dart';

import '../../utils/constants.dart';

class SocialButton extends StatelessWidget {
  final Widget _icon;
  final VoidCallback? _action;

  const SocialButton(this._icon, this._action, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double bh = Constants.buttonHeight;
    return OutlinedButton(
        onPressed: _action,
        style: ButtonStyle(
          minimumSize: MaterialStateProperty.all(
            Size(bh, bh),
          ),
          maximumSize: MaterialStateProperty.all(
            Size(bh, bh),
          ),
        ),
        child: _icon);
  }
}
