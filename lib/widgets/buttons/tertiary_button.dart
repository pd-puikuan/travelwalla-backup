import 'package:flutter/material.dart';
import 'package:travelwalla_mobile/utils/size_config.dart';

class TertiaryButton extends StatelessWidget {
  final String _label;
  final String _route;

  const TertiaryButton(this._label, this._route, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return TextButton(
      onPressed: () => Navigator.pushNamed(context, _route),
      child: Text(_label, style: TextStyle(fontSize: SizeConfig.buttonFontSize)),
    );
  }
}