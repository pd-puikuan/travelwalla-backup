import 'package:flutter/material.dart';

import '../../widgets/chips/offer_chip.dart';
import '../../utils/size_config.dart';

class BookingCard extends StatelessWidget {
  final String image;
  final String title;
  final String subtitle;
  final String desc;
  final String offer;

  const BookingCard({
    Key? key,
    required this.image,
    required this.title,
    this.subtitle = '',
    required this.desc,
    this.offer = '',
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flex(
      direction: Axis.horizontal,
      children: [
        Expanded(
          flex: 3,
          child: FittedBox(
            fit: BoxFit.fitWidth,
            child: Container(
                margin: EdgeInsets.only(right: SizeConfig.smallMargin),
                decoration:
                    BoxDecoration(borderRadius: BorderRadius.circular(10.0)),
                clipBehavior: Clip.hardEdge,
                child: Image.asset(
                  image,
                  fit: BoxFit.cover,
                  height: SizeConfig.safeBlockHorizontal * 25,
                  width: SizeConfig.safeBlockHorizontal * 25,
                )),
          ),
        ),
        Expanded(
          flex: 5,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(title,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    height: SizeConfig.textHeight,
                  )),
              subtitle != '' ? Text(subtitle,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    height: SizeConfig.textHeight,
                  )) : const SizedBox.shrink(),
              Text(desc, style: TextStyle(height: SizeConfig.textHeight)),
              offer != '' ? OfferChip(offer) : const SizedBox.shrink(),
            ],
          ),
        ),
      ],
    );
  }
}
