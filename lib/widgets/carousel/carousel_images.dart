import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'carousel_indicator.dart';

// ignore: must_be_immutable
class CarouselImages extends StatefulWidget {
  List<Widget> imageSet;

  CarouselImages(this.imageSet, {Key? key}) : super(key: key);

  @override
  State<CarouselImages> createState() => _CarouselImagesState();
}

class _CarouselImagesState extends State<CarouselImages> {
  int _current = 0;
  final CarouselController _controller = CarouselController();

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: AlignmentDirectional.bottomStart,
      children: [
        CarouselSlider(
          options: CarouselOptions(
              scrollDirection: Axis.horizontal,
              viewportFraction: 1.0,
              onPageChanged: (index, reason) {
                setState(() {
                  _current = index;
                });
              }),
          items: widget.imageSet,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: widget.imageSet.asMap().entries.map((entry) {
            return CarouselIndicator(_controller, entry.key, _current);
          }).toList(),
        ),
      ],
    );
  }
}
