import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

// ignore: must_be_immutable
class CarouselIndicator extends StatelessWidget {
  CarouselController controller;
  int currentKey;
  int currentIndex;

  CarouselIndicator(this.controller, this.currentKey, this.currentIndex,
      {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => controller.animateToPage(currentKey),
      child: Container(
        width: 8.0,
        height: 8.0,
        margin: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 4.0),
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: (currentIndex == currentKey
                ? Theme.of(context).primaryColor
                : Colors.white)),
      ),
    );
  }
}
