import 'package:flutter/material.dart';

import '../../utils/strings.dart';
import '../../utils/size_config.dart';
import '../../utils/extensions.dart';

class BookingChip extends StatelessWidget {
  final String _bookingId;
  final int _points;
  const BookingChip(this._bookingId, this._points, {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.symmetric(vertical: SizeConfig.smallMargin),
          child: Chip(
            label: Text(
              '${Strings.paymentBookingId}: $_bookingId',
              style: TextStyle(color: Theme.of(context).primaryColor),
            ),
            backgroundColor: Theme.of(context).primaryColor.withOpacity(0.1),
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(5))),
          ),
        ),
        RichText(
          text: TextSpan(
            style: TextStyle(
                color: Theme.of(context).colorScheme.secondary,
                fontWeight: FontWeight.bold),
            children: [
              WidgetSpan(
                child: Icon(Icons.star,
                    size: SizeConfig.mediumFontSize,
                    color: Theme.of(context).colorScheme.secondary),
              ),
              TextSpan(
                text:
                    '+$_points ${Strings.bookingUseRewardPoints.toTitleCase()}',
              ),
            ],
          ),
        )
      ],
    );
  }
}
