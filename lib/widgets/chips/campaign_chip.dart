import 'package:flutter/material.dart';
import '../../utils/size_config.dart';

// ignore: must_be_immutable
class CampaignChipSet extends StatelessWidget {
  List<String> campaigns;

  CampaignChipSet(this.campaigns, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return Align(
      alignment: Alignment.topLeft,
      child: Wrap(
        alignment: WrapAlignment.start,
        runAlignment: WrapAlignment.start,
        runSpacing: 6.0,
        // spacing: 0.5,
        children: List<Widget>.generate(
          campaigns.length,
          (int index) {
            return FittedBox(
              fit: BoxFit.fitWidth,
              child: Container(
                margin:
                    EdgeInsets.only(right: SizeConfig.safeBlockVertical * 0.5),
                padding: EdgeInsets.all(SizeConfig.xSmallPadding),
                decoration: BoxDecoration(
                    borderRadius: const BorderRadius.all(Radius.circular(5)),
                    color: Theme.of(context).primaryColor.withOpacity(0.2)),
                child: RichText(
                  text: TextSpan(children: [
                    WidgetSpan(
                      child: Icon(Icons.tag,
                          size: SizeConfig.safeBlockVertical * 2.0,
                          color: Theme.of(context).primaryColor),
                    ),
                    TextSpan(
                        text: campaigns[index],
                        style: TextStyle(
                            fontSize: SizeConfig.xSmallFontSize,
                            color: Theme.of(context).colorScheme.primary))
                  ]),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
