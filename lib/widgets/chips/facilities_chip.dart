import 'package:flutter/material.dart';
import '../../utils/size_config.dart';

// ignore: must_be_immutable
class FacilitiesChipSet extends StatelessWidget {
  List<Set> facilities;

  FacilitiesChipSet(this.facilities, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return Align(
      alignment: Alignment.topLeft,
      child: Wrap(
        alignment: WrapAlignment.start,
        runAlignment: WrapAlignment.start,
        runSpacing: 6.0,
        // spacing: 0.5,
        children: List<Widget>.generate(
          facilities.length,
          (int index) {
            return Container(
              margin:
                  EdgeInsets.only(right: SizeConfig.safeBlockVertical * 1),
              padding: EdgeInsets.all(SizeConfig.xSmallPadding),
              child: RichText(
                text: TextSpan(children: [
                  WidgetSpan(
                    child: Container(
                      margin: EdgeInsets.only(right: SizeConfig.xSmallMargin),
                      child: Icon(facilities[index].elementAt(0),
                          size: SizeConfig.safeBlockVertical * 2.0,
                          color: Colors.black),
                    ),
                  ),
                  TextSpan(
                      text: facilities[index].elementAt(1),
                      style: TextStyle(
                          fontSize: SizeConfig.smallFontSize, color: Colors.black))
                ]),
              ),
            );
          },
        ),
      ),
    );
  }
}
