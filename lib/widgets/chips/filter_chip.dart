import 'package:flutter/material.dart';
import '../../utils/size_config.dart';

// ignore: must_be_immutable
class FilterChipSet extends StatelessWidget {
  List<String> filters;

  FilterChipSet(this.filters, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Align(
      alignment: Alignment.topLeft,
      child: Wrap(
        alignment: WrapAlignment.start,
        runAlignment: WrapAlignment.spaceAround,
        // spacing: 6.0,
        runSpacing: 5.0,
        children: List<Widget>.generate(
          filters.length,
          (int index) {
            return Container(
                margin:
                    EdgeInsets.only(right: SizeConfig.safeBlockVertical * 0.5),
                child: Chip(
                    label: Text(filters[index]),
                    padding:
                        EdgeInsets.all(SizeConfig.safeBlockVertical * 1.25)));
          },
        ),
      ),
    );
  }
}
