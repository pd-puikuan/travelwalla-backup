import 'package:flutter/material.dart';

import '../../utils/size_config.dart';

class OfferChip extends StatelessWidget {
  final String _offer;

  const OfferChip(this._offer, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
                margin: EdgeInsets.symmetric(vertical: SizeConfig.smallMargin),
                padding: EdgeInsets.all(SizeConfig.xSmallPadding),
                decoration: BoxDecoration(
                    color: Theme.of(context)
                        .colorScheme
                        .tertiary
                        .withOpacity(0.15),
                    borderRadius: BorderRadius.circular(5.0)),
                child: Text(_offer,
                    style: TextStyle(
                        fontSize: SizeConfig.xSmallFontSize,
                        color: Theme.of(context).colorScheme.tertiary)),
              );
  }
}