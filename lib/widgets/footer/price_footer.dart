import 'package:flutter/material.dart';

import '../../widgets/buttons/primary_button.dart';

import '../../utils/strings.dart';
import '../../utils/routes.dart';
import '../../utils/size_config.dart';

class PriceFooter extends StatelessWidget {
  final double price;
  final int footerHeight;
  final bool isSummary;

  const PriceFooter({
    Key? key,
    required this.price,
    required this.footerHeight,
    this.isSummary = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
          alignment: Alignment.bottomCenter,
          padding: SizeConfig.safeContainerMargin.copyWith(bottom: 0),
          height: SizeConfig.safeBlockVertical * footerHeight,
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.shade200,
                blurRadius: 2,
                offset: const Offset(0, -4), // Shadow position
              ),
            ],
          ),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Flex(
              direction: Axis.horizontal,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      Strings.bookingTotalPrice.toUpperCase(),
                      style: TextStyle(
                        fontSize: SizeConfig.subtitleFontSize,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      Strings.bookingTotalPriceText,
                      style: TextStyle(
                          fontSize: SizeConfig.smallFontSize,
                          color: Colors.grey),
                    ),
                  ],
                ),
                Text(
                  '${Strings.currency} ${price.toStringAsFixed(2)}',
                  style: TextStyle(
                    fontSize: SizeConfig.subtitleFontSize,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
            isSummary ? Column(
              children: [
                Container(
                  margin: EdgeInsets.symmetric(vertical: SizeConfig.mediumMargin),
                  child: Text(
                    Strings.bookingTnc,
                    style: TextStyle(height: SizeConfig.textHeight),
                  ),
                ),
                PrimaryButton(Strings.bookingPaymentButton, () => Navigator.pushNamed(context, Routes.hotelPayment)),
              ],
            ) : const SizedBox.shrink(),
          ])),
    );
  }
}
