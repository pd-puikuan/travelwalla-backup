import 'package:flutter/material.dart';

import '../../utils/size_config.dart';

class IconOption extends StatefulWidget {
  final String _label;
  final IconData _icon;
  const IconOption(this._label, this._icon, {Key? key}) : super(key: key);

  @override
  State<IconOption> createState() => _IconOptionState();
}

class _IconOptionState extends State<IconOption> {
  var active = false;

  updateStatus() {
    setState(() {
      active != active;
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Expanded(
      child: GestureDetector(
        onTap: () => updateStatus(),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: SizeConfig.smallMargin),
          padding: EdgeInsets.symmetric(
              vertical: SizeConfig.largePadding,
              horizontal: SizeConfig.smallPadding),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.grey),
            borderRadius: BorderRadius.circular(5.0),
            color:
                active ? Theme.of(context).colorScheme.primary : Colors.white,
          ),
          child: Column(
            children: [
              Icon(widget._icon,
                  size: SizeConfig.largeFontSize,
                  color: active ? Colors.white : Colors.black),
              FittedBox(
                  fit: BoxFit.fitWidth,
                  child: Text(widget._label,
                      style: TextStyle(
                          color: active ? Colors.white : Colors.black))),
            ],
          ),
        ),
      ),
    );
  }
}
