import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../models/search.dart';
import '../../utils/size_config.dart';

class NumericInput extends StatefulWidget {
  final String _label;
  final int _value;
  final String _type;

  const NumericInput(this._label, this._value, this._type, {Key? key})
      : super(key: key);

  @override
  State<NumericInput> createState() => _NumericInputState();
}

class _NumericInputState extends State<NumericInput> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.symmetric(vertical: SizeConfig.mediumMargin),
          child: Flex(
            direction: Axis.horizontal,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                flex: 1,
                child: Text(widget._label),
              ),
              Flexible(
                  flex: 1,
                  child: Consumer<SearchModel>(
                    builder: (context, value, child) {
                      return RichText(
                          text: TextSpan(children: [
                        WidgetSpan(
                            alignment: PlaceholderAlignment.middle,
                            child: GestureDetector(
                              onTap: () => Provider.of<SearchModel>(context,
                                      listen: false)
                                  .updateAmount(widget._type, -1),
                              child: Container(
                                margin: EdgeInsets.only(
                                    right: SizeConfig.mediumMargin),
                                child: Icon(Icons.remove_circle_outline,
                                    size: SizeConfig.largeFontSize),
                              ),
                            )),
                        TextSpan(
                            text: widget._value.toString(),
                            style: const TextStyle(color: Colors.black)),
                        WidgetSpan(
                            alignment: PlaceholderAlignment.middle,
                            child: GestureDetector(
                              onTap: () => Provider.of<SearchModel>(context,
                                      listen: false)
                                  .updateAmount(widget._type, 1),
                              child: Container(
                                margin: EdgeInsets.only(
                                    left: SizeConfig.mediumMargin),
                                child: Icon(Icons.add_circle_outline,
                                    size: SizeConfig.largeFontSize),
                              ),
                            )),
                      ]));
                    },
                  ))
            ],
          ),
        ),
        const Divider(),
      ],
    );
  }
}
