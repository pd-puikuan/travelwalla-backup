import 'package:flutter/material.dart';
import '../../utils/size_config.dart';

class TextInput extends StatelessWidget {
  final TextEditingController controller;
  final bool enablePrefix;
  final bool enableSuffix;
  final dynamic prefix;
  final dynamic suffix;
  final String label;
  final bool readOnly;
  final AutovalidateMode autovalidateMode;
  final VoidCallback? action;

  const TextInput({
    Key? key,
    required this.controller,
    this.enablePrefix = false,
    this.enableSuffix = false,
    this.prefix,
    this.suffix,
    this.readOnly = false,
    this.autovalidateMode = AutovalidateMode.always,
    required this.label,
    this.action,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    
    SizeConfig().init(context);
    return Container(
      margin: EdgeInsets.symmetric(vertical: SizeConfig.smallMargin),
      child: TextFormField(
        controller: controller,
        readOnly: readOnly,
        autovalidateMode: autovalidateMode,
        decoration: InputDecoration(
          prefixIcon: enablePrefix ? prefix : null,
          suffixIcon: enableSuffix ? suffix : null,
          contentPadding: const EdgeInsets.all(20.0),
          border: const OutlineInputBorder(),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: Colors.grey.shade400),
          ),
          label: Text(label),
        ),
        onTap: action,
      ),
    );
  }
}
