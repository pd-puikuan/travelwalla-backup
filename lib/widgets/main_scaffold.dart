import 'package:flutter/material.dart';

import '../utils/size_config.dart';

class MainScaffold extends StatelessWidget {
  final Widget child;
  final String title;
  final bool extendBack;
  final Widget? footer;

  const MainScaffold({Key? key, required this.child, this.title = '', this.extendBack = false, this.footer})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          title,
          style: TextStyle(fontSize: SizeConfig.mediumFontSize),
        ),
      ),
      extendBodyBehindAppBar: extendBack,
      body: SingleChildScrollView(
        child: Container(
          // height: MediaQuery.of(context).size.height,
          margin: SizeConfig.safeContainerMargin,
          child: child,
        ),
      ),
      bottomSheet: footer,
    );
  }
}
