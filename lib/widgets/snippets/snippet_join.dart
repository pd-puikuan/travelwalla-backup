import 'package:flutter/material.dart';

import '../../utils/strings.dart';
import '../../utils/size_config.dart';

class SnippetJoin extends StatelessWidget {
  const SnippetJoin({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(Strings.loginHeader1,
            style: TextStyle(
                fontSize: SizeConfig.semiLargeFontSize,
                fontWeight: FontWeight.w600)),
        Text(Strings.loginHeader2,
            style: TextStyle(
                fontSize: SizeConfig.semiLargeFontSize,
                color: Theme.of(context).colorScheme.secondary,
                fontWeight: FontWeight.w600)),
        const SizedBox(height: 10),
      ],
    );
  }
}
