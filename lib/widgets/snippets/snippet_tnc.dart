import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../utils/strings.dart';
import '../../utils/size_config.dart';

class SnippetTnc extends StatelessWidget {
  const SnippetTnc({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      margin: EdgeInsets.all(SizeConfig.mediumMargin),
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          style: TextStyle(height: SizeConfig.textHeight),
          children: [
            TextSpan(
                text: Strings.loginTncText,
                style: const TextStyle(color: Colors.black)),
            TextSpan(
                recognizer: TapGestureRecognizer()
                  ..onTap = () async {
                    if (await canLaunchUrl(
                            Uri.parse("https://www.google.com")) ==
                        true) {
                      launchUrl(Uri.parse("https://www.google.com"));
                    } else {
                      // print("Can't launch URL");
                    }
                  },
                text: ' ${Strings.loginTermsOfService} ',
                style: TextStyle(
                    color: Theme.of(context).colorScheme.primary,
                    fontWeight: FontWeight.bold)),
            TextSpan(
                text: Strings.and, style: const TextStyle(color: Colors.black)),
            TextSpan(
                recognizer: TapGestureRecognizer()
                  ..onTap = () async {
                    if (await canLaunchUrl(
                            Uri.parse("https://www.google.com")) ==
                        true) {
                      launchUrl(Uri.parse("https://www.google.com"));
                    } else {
                      // print("Can't launch URL");
                    }
                  },
                text: ' ${Strings.loginPrivacyPolicy}',
                style: TextStyle(
                    color: Theme.of(context).colorScheme.primary,
                    fontWeight: FontWeight.bold)),
            const TextSpan(text: '.', style: TextStyle(color: Colors.black)),
          ],
        ),
      ),
    );
  }
}
