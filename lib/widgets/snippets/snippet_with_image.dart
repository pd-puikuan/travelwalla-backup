import 'package:flutter/material.dart';

import '../../utils/size_config.dart';

class SnippetWithImage extends StatelessWidget {
  final String imageUri;
  final String title;
  final String subtitle;
  final String desc;
  final bool altView;

  const SnippetWithImage({
    Key? key,
    required this.imageUri,
    required this.title,
    this.subtitle = '',
    required this.desc,
    this.altView = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    var subtitleSection = Text(
      subtitle,
      style: TextStyle(
        fontSize: SizeConfig.largeFontSize,
      ),
    );

    var descSection = Text(
      desc,
      textAlign: TextAlign.center,
      style: TextStyle(height: SizeConfig.textHeight),
    );

    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(top: SizeConfig.largeMargin),
          padding: EdgeInsets.all(SizeConfig.mediumPadding),
          child: Image.asset(imageUri),
        ),
        Text(title,
            style: TextStyle(
                fontSize: SizeConfig.headerFontSize,
                fontWeight: FontWeight.bold,
                color: Theme.of(context).colorScheme.secondary)),
        Container(
          margin: EdgeInsets.symmetric(vertical: SizeConfig.mediumMargin),
          child: !altView ? descSection : subtitleSection,
        ),
        !altView ? subtitleSection : descSection,
      ],
    );
  }
}
