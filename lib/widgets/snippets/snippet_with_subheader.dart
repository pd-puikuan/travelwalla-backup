import 'package:flutter/material.dart';

import '../../utils/size_config.dart';

class SnippetWithSubheader extends StatelessWidget {
  final String title;
  final String subtitle;
  final String desc;
  final VoidCallback? action;

  const SnippetWithSubheader(
      {Key? key,
      required this.title,
      required this.subtitle,
      this.desc = '',
      this.action})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 10),
        const Divider(),
        Flex(
          direction: Axis.horizontal,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              title,
              style: TextStyle(
                fontSize: SizeConfig.subtitleFontSize,
                fontWeight: FontWeight.bold,
                color: Theme.of(context).colorScheme.secondary,
                height: SizeConfig.textHeight,
              ),
            ),
            action != null
                ? GestureDetector(
                  onTap: action,
                  child: Text('$title >',
                      style: TextStyle(
                          fontSize: SizeConfig.smallFontSize,
                          color: Theme.of(context).colorScheme.primary,
                          height: SizeConfig.textHeight,)),
                )
                : const SizedBox.shrink(),
          ],
        ),
        Text(
          subtitle,
          style: TextStyle(
            fontSize: SizeConfig.subtitleFontSize,
            fontWeight: FontWeight.bold,
            height: SizeConfig.textHeight,
          ),
        ),
        desc != ''
            ? Text(desc, style: TextStyle(height: SizeConfig.textHeight))
            : const SizedBox.shrink(),
      ],
    );
  }
}
